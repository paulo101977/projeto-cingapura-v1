$(document).ready(function(){

  //Combo box for multiple products - START
  var valorSelecionado;
  var tituloProduto;
  var terminalProduto;
  var tituloProdutoCutted;

if ($("select[id^='select-produto-']").length > 0){
  $("select[id^='select-produto-']").selectbox({
    onOpen : function (inst) {
      //$('.sbHolder').css('border-color' , '#887bbf');
      //$.getScript("/ArquivosEstaticos/MinhaOi/js/minhaoi.js");
	  	$("select[id^='select-produto-'] option").removeAttr('selected');
    },
    onChange : function (value, inst) {
      
      // Altera o valor do select vanilla - ANDERSON - PKE22605 
      var valorSelecionado           = value;
      var instanciaSelectBoxOriginal = $("#"+inst.id);

      instanciaSelectBoxOriginal.val(value);
      instanciaSelectBoxOriginal.trigger("change");
     

      var selectboxId = inst.id,
      selectboxSelected= instanciaSelectBoxOriginal;
        
      // Altera o valor do select vanilla - ANDERSON - PKE22605 - END


     
      var idLabelOptSelected = $('#' + selectboxId + ' option:selected').text();
      var aux = carregaSelectbox(idLabelOptSelected, true);
      var containerSelect = $(this).parent().parent();
      var labelOptSelected = '';
      var optSelected;

      if (selectboxId == 'select-produto-combo') {

        //Mudar o link do botão - ANDERSON - PKE22605 

                var linkAnterior    = $("#detalhe-combo-0").attr("href");
                var paramsStr       = valorSelecionado.split("?")[1];
                //Remover # da URL
                paramsStr           = paramsStr.split("#")[0];
                var variaveisDaUrl  = getParamsFromUrl(paramsStr);
                var novaUrlBotao    = linkAnterior.split("?")[0] + "?" + $.param( variaveisDaUrl );
                $("#detalhe-combo-0").attr("href", novaUrlBotao);

        //Mudar o link do botão - ANDERSON - PKE22605  - END

        if (isSafari()) {
          $('#select-produto-combo option').each(function(){

            var seletorSafari = $(this).attr('selected');

            if (typeof seletorSafari !== typeof undefined && seletorSafari !== false) {
              labelOptSelected = $(this).text();
            }

          });

        } else {
          labelOptSelected = $('#select-produto-combo option:selected').text();
        }

        $('#box-combo .box-produto .box-header .sbHolder a.sbSelector').html(
          aux + '<small class="text-muted pull-right">' + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)  + '</small>'
        );

        var urlOptSelected = $('#' + selectboxId + ' option:selected').val();
        $('#select-produto-combo').parent().find('div a').attr('href', urlOptSelected);

        // Faz o ajax funcionar
        $('div[id^="div_oct_"]').hide();
        var linkhref = $('#select-produto-combo').parent().find('div a.sbSelector').attr('href').toString();

        linkhref = linkhref.split('#');

        var destdiv = $('#' + linkhref[1]);
        destdiv.show();
        if (linkhref[0] != '' && escape(linkhref[0].split('/').pop()) != window.location.href.split('/').pop().split('#')[0]) {
          destdiv.html('<div class="loadingAjax"><p>Carregando conte&uacute;do...</p></div>');

          $.ajax({
            url: linkhref[0],
            dataType: 'html',
            type: 'post',
            async: true,
            success: function(dados,textStatus, request) {

              // Preenche o ajax da home inicio
              $(destdiv).empty().append(innerShiv(dados, false));
              menuLateralInit();
              carregaTooltipHover('#bt-segunda-via', 80, 65, 'arrow-box tooltip-inner simple-text');
              carregaTooltipHover('#bt-segunda-via-link', 80, 65, 'arrow-box tooltip-inner simple-text small-link-list');

            }
          });

        }

        optSelected = containerSelect.find('#box-combo a[id^="sbSelector"]').html();

      } else if (selectboxId == 'select-produto-movel') {


        //Mudar o link do botão - ANDERSON - PKE22605 

                var linkAnterior    = $("#detalhe-movel-0").attr("href");
                var paramsStr       = valorSelecionado.split("?")[1];
                //Remover # da URL
                paramsStr           = paramsStr.split("#")[0];
                var variaveisDaUrl  = getParamsFromUrl(paramsStr);
                var novaUrlBotao    = linkAnterior.split("?")[0] + "?" + $.param( variaveisDaUrl );
                $("#detalhe-movel-0").attr("href", novaUrlBotao);

        //Mudar o link do botão - ANDERSON - PKE22605  - END

        if (isSafari()) {
          $('#select-produto-movel option').each(function(){

            var seletorSafari = $(this).attr('selected');

            if (typeof seletorSafari !== typeof undefined && seletorSafari !== false) {
              labelOptSelected = $(this).text();
            }

          });

        } else {
          labelOptSelected = $('#select-produto-movel option:selected').text();
        }

        $('#box-movel .box-produto .box-header .sbHolder a.sbSelector').html(
          aux + '<small class="text-muted pull-right">' + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)  + '</small>'
        );

        var urlOptSelected = $('#' + selectboxId + ' option:selected').val();
        $('#select-produto-movel').parent().find('div a').attr('href', urlOptSelected);

        // Faz o ajax funcionar
        $('div[id^="div_mvl_"]').hide();
        var linkhref = $('#select-produto-movel').parent().find('div a.sbSelector').attr('href').toString();

        linkhref = linkhref.split('#');

        var destdiv = $('#' + linkhref[1]);
        destdiv.show();
        if (linkhref[0] != '' && escape(linkhref[0].split('/').pop()) != window.location.href.split('/').pop().split('#')[0]) {
          destdiv.html('<div class="loadingAjax"><p>Carregando conte&uacute;do...</p></div>');

          $.ajax({
            url: linkhref[0],
            dataType: 'html',
            type: 'post',
            async: true,
            success: function(dados,textStatus, request) {
              // Preenche o ajax da home inicio
              $(destdiv).empty().append(innerShiv(dados, false));
              menuLateralInit();
              carregaTooltipHover('#bt-segunda-via', 80, 65, 'arrow-box tooltip-inner simple-text');
              carregaTooltipHover('#bt-segunda-via-link', 80, 65, 'arrow-box tooltip-inner simple-text small-link-list');

              createTooltipIcos();
            }
          });
        }

        optSelected = containerSelect.find('#box-movel a[id^="sbSelector"]').html();

      } else if (selectboxId == 'select-produto-fixo') {

        if (isSafari()) {
          $('#select-produto-fixo option').each(function(){

            var seletorSafari = $(this).attr('selected');

            if (typeof seletorSafari !== typeof undefined && seletorSafari !== false) {
              labelOptSelected = $(this).text();
            }

          });

        } else {
          labelOptSelected = $('#select-produto-fixo option:selected').text();
        }

       $('#box-fixo .box-produto .box-header .sbHolder a.sbSelector').html(
          aux + '<small class="text-muted pull-right">' + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)  + '</small>'
        );

        var urlOptSelected = $('#' + selectboxId + ' option:selected').val();
        $('#select-produto-fixo').parent().find('div a').attr('href', urlOptSelected);

        // Faz o ajax funcionar
        $('div[id^="div_fxo_"]').hide();
        var linkhref = $('#select-produto-fixo').parent().find('div a.sbSelector').attr('href').toString();

        linkhref = linkhref.split('#');

        var destdiv = $('#' + linkhref[1]);
        destdiv.show();
        if (linkhref[0] != '' && escape(linkhref[0].split('/').pop()) != window.location.href.split('/').pop().split('#')[0]) {
          destdiv.html('<div class="loadingAjax"><p>Carregando conte&uacute;do...</p></div>');

          $.ajax({
            url: linkhref[0],
            dataType: 'html',
            type: 'post',
            async: true,
            success: function(dados,textStatus, request) {
              // Preenche o ajax da home inicio
              $(destdiv).empty().append(innerShiv(dados, false));
              menuLateralInit();
              carregaTooltipHover('#bt-segunda-via', 80, 65, 'arrow-box tooltip-inner simple-text');
              carregaTooltipHover('#bt-segunda-via-link', 80, 65, 'arrow-box tooltip-inner simple-text small-link-list');
            }
          });

        }

        optSelected = containerSelect.find('#box-fixo a[id^="sbSelector"]').html();

      } else if (selectboxId == 'select-produto-internet') {

        if (isSafari()) {
          $('#select-produto-internet option').each(function(){

            var seletorSafari = $(this).attr('selected');

            if (typeof seletorSafari !== typeof undefined && seletorSafari !== false) {
              labelOptSelected = $(this).text();
            }

          });

        } else {
          labelOptSelected = $('#select-produto-internet option:selected').text();
        }

        $('#box-internet .box-produto .box-header .sbHolder a.sbSelector').html(
           aux + '<small class="text-muted pull-right">' + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)  + '</small>'
        );

        var urlOptSelected = $('#' + selectboxId + ' option:selected').val();
        $('#select-produto-internet').parent().find('div a').attr('href', urlOptSelected);

        // Faz o ajax funcionar
        $('div[id^="div_int_"]').hide();
        var linkhref = $('#select-produto-internet').parent().find('div a.sbSelector').attr('href').toString();

        linkhref = linkhref.split('#');

        var destdiv = $('#' + linkhref[1]);
        destdiv.show();
        if (linkhref[0] != '' && escape(linkhref[0].split('/').pop()) != window.location.href.split('/').pop().split('#')[0]) {
          destdiv.html('<div class="loadingAjax"><p>Carregando conte&uacute;do...</p></div>');

          $.ajax({
            url: linkhref[0],
            dataType: 'html',
            type: 'post',
            async: true,
            success: function(dados,textStatus, request) {
              // Preenche o ajax da home inicio
              $(destdiv).empty().append(innerShiv(dados, false));
              menuLateralInit();
              carregaTooltipHover('#bt-segunda-via', 80, 65, 'arrow-box tooltip-inner simple-text');
              carregaTooltipHover('#bt-segunda-via-link', 80, 65, 'arrow-box tooltip-inner simple-text small-link-list');
            }
          });

        }

        optSelected = containerSelect.find('#box-internet a[id^="sbSelector"]').html();

      } else if (selectboxId == 'select-produto-tv') {

        if (isSafari()) {
          $('#select-produto-tv option').each(function(){

            var seletorSafari = $(this).attr('selected');

            if (typeof seletorSafari !== typeof undefined && seletorSafari !== false) {
              labelOptSelected = $(this).text();
            }

          });

        } else {
          labelOptSelected = $('#select-produto-tv option:selected').text();
        }

        $('#box-tv .box-produto .box-header .sbHolder a.sbSelector').html(
          aux + '<small class="text-muted pull-right">' + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)  + '</small>'
        );

        var urlOptSelected = $('#' + selectboxId + ' option:selected').val();
        $('#select-produto-tv').parent().find('div a').attr('href', urlOptSelected);

        // Faz o ajax funcionar
        $('div[id^="div_ptv_"]').hide();
        var linkhref = $('#select-produto-tv').parent().find('div a.sbSelector').attr('href').toString();

        linkhref = linkhref.split('#');

        var destdiv = $('#' + linkhref[1]);
        destdiv.show();
        if (linkhref[0] != '' && escape(linkhref[0].split('/').pop()) != window.location.href.split('/').pop().split('#')[0]) {
          destdiv.html('<div class="loadingAjax"><p>Carregando conte&uacute;do...</p></div>');

          $.ajax({
            url: linkhref[0],
            dataType: 'html',
            type: 'post',
            async: true,
            success: function(dados,textStatus, request) {
              // Preenche o ajax da home inicio
              $(destdiv).empty().append(innerShiv(dados, false));
              menuLateralInit();
              carregaTooltipHover('#bt-segunda-via', 80, 65, 'arrow-box tooltip-inner simple-text');
              carregaTooltipHover('#bt-segunda-via-link', 80, 65, 'arrow-box tooltip-inner simple-text small-link-list');
            }
          });

        }

        optSelected = containerSelect.find('#box-tv a[id^="sbSelector"]').html();

      }

      //console.log('Opcao selecionada lista  ' + optSelected);

      $('#' + selectboxId + ' option').each(function (){

        if($(this).val() == optSelected) {
          //console.log('Opcao selecionada   ' + $(this).val());
          $(this).prop('selected', true);
        }
      });

    },
    onClose : function (inst) {
      $('.sbHolder').css('border-color' , '#ddd');

      //$.getScript("/ArquivosEstaticos/MinhaOi/js/minhaoi.js");
    }
  });
}

  //Verifies the size of the selected option to apply the layout
  valoresSelecionados = $("select[id^='select-produto-']");

  if( valoresSelecionados != null || valoresSelecionados != undefined ){

    valoresSelecionados.each(function(){

      //var valorSelecionado = $(this).val();
      var valorSelecionado = $(this).text();
      var selectboxId = $(this).attr("id");
      var idLabelOptSelected = $('#' + selectboxId + ' option:selected').text();
      var tituloValorSelecionado = carregaSelectbox(idLabelOptSelected, true);
      var telefoneValorSelecionado;
      var labelOptSelected;
      var optsPlanos;

      //console.log("tituloValorSelecionado " + tituloValorSelecionado);
      //console.log("telefoneValorSelecionado" + telefoneValorSelecionado);

      if ($(this).attr('id') == 'select-produto-combo') {

        labelOptSelected = $('#select-produto-combo option:selected').text();

        telefoneValorSelecionado = '<small class="text-muted pull-right">'
                                      + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)
                                      +'</small>';

        $('#box-combo .box-produto .box-header .sbHolder a.sbSelector').html(tituloValorSelecionado + telefoneValorSelecionado);

        optsPlanos = $('#box-combo .box-produto .box-header .sbHolder .sbOptions li');
        optsPlanos.each(function() {

          var opcao = $(this).find("a").html();
          //console.log("teste " + str.indexOf("("));
          //var str1 = strFull.slice(0,strFull.indexOf("(")-1);
          var tituloOpcao = carregaSelectbox(opcao, false);
          var telefoneOpcao = '<small class="text-muted pull-right">'+ opcao.slice(opcao.indexOf("("), opcao.length)+'</small>';
          $(this).find("a").html(tituloOpcao + telefoneOpcao);

        });

      } else if ($(this).attr('id') == 'select-produto-movel') {

        labelOptSelected = $('#select-produto-movel option:selected').text();

        telefoneValorSelecionado = '<small class="text-muted pull-right">'
                                      + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)
                                      +'</small>';

        $('#box-movel .box-produto .box-header .sbHolder a.sbSelector').html(tituloValorSelecionado + telefoneValorSelecionado);

        optsPlanos = $('#box-movel .box-produto .box-header .sbHolder .sbOptions li');
        optsPlanos.each(function() {

          var opcao = $(this).find("a").html();
          //console.log("teste " + str.indexOf("("));
          //var str1 = strFull.slice(0,strFull.indexOf("(")-1);
          var tituloOpcao = carregaSelectbox(opcao, false);
          var telefoneOpcao = '<small class="text-muted pull-right">'+ opcao.slice(opcao.indexOf("("), opcao.length)+'</small>';
          $(this).find("a").html(tituloOpcao + telefoneOpcao);

        });

      } else if ($(this).attr('id') == 'select-produto-fixo') {

        labelOptSelected = $('#select-produto-fixo option:selected').text();

        telefoneValorSelecionado = '<small class="text-muted pull-right">'
                                      + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)
                                      +'</small>';

        $('#box-fixo .box-produto .box-header .sbHolder a.sbSelector').html(tituloValorSelecionado + telefoneValorSelecionado);

        optsPlanos = $('#box-fixo .box-produto .box-header .sbHolder .sbOptions li');
        optsPlanos.each(function() {

          var opcao = $(this).find("a").html();
          //console.log("teste " + str.indexOf("("));
          //var str1 = strFull.slice(0,strFull.indexOf("(")-1);
          var tituloOpcao = carregaSelectbox(opcao, false);
          var telefoneOpcao = '<small class="text-muted pull-right">'+ opcao.slice(opcao.indexOf("("), opcao.length)+'</small>';
          $(this).find("a").html(tituloOpcao + telefoneOpcao);

        });

      } else if ($(this).attr('id') == 'select-produto-internet') {

        labelOptSelected = $('#select-produto-internet option:selected').text();

        telefoneValorSelecionado = '<small class="text-muted pull-right">'
                                      + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)
                                      +'</small>';

        $('#box-internet .box-produto .box-header .sbHolder a.sbSelector').html(tituloValorSelecionado + telefoneValorSelecionado);

        optsPlanos = $('#box-internet .box-produto .box-header .sbHolder .sbOptions li');
        optsPlanos.each(function() {

          var opcao = $(this).find("a").html();
          //console.log("teste " + str.indexOf("("));
          //var str1 = strFull.slice(0,strFull.indexOf("(")-1);
          var tituloOpcao = carregaSelectbox(opcao, false);
          var telefoneOpcao = '<small class="text-muted pull-right">'+ opcao.slice(opcao.indexOf("("), opcao.length)+'</small>';
          $(this).find("a").html(tituloOpcao + telefoneOpcao);

        });

      } else if ($(this).attr('id') == 'select-produto-tv') {

        labelOptSelected = $('#select-produto-tv option:selected').text();

        telefoneValorSelecionado = '<small class="text-muted pull-right">'
                                      + labelOptSelected.slice(labelOptSelected.indexOf("("), labelOptSelected.length)
                                      +'</small>';

        $('#box-tv .box-produto .box-header .sbHolder a.sbSelector').html(tituloValorSelecionado + telefoneValorSelecionado);

        optsPlanos = $('#box-tv .box-produto .box-header .sbHolder .sbOptions li');
        optsPlanos.each(function() {

          var opcao = $(this).find("a").html();
          //console.log("teste " + str.indexOf("("));
          //var str1 = strFull.slice(0,strFull.indexOf("(")-1);
          var tituloOpcao = carregaSelectbox(opcao, false);
          var telefoneOpcao = '<small class="text-muted pull-right">'+ opcao.slice(opcao.indexOf("("), opcao.length)+'</small>';
          $(this).find("a").html(tituloOpcao + telefoneOpcao);

        });

      }

    });
  }
  //Combo box for multiple products - END

  //Chama os selectboxes antigos - Start
  if ($('.sweetContentSelect, .sweetSelectBox, .selectbox, .sweetEstadoSelect, .selectfiltro').length > 0) {
    $('.sweetContentSelect, .sweetSelectBox, .selectbox, .sweetEstadoSelect, .selectfiltro').selectbox();
	}
    $(".cfbancoCEF").hide(); /* combo adesao DACC */
    
  //Chama os selectboxes antigos - End

  //4740_4604 FALE CONOSCO -START
  //Loads custom selectboxes
  if ($(".fale-conosco #select-produto").length > 0){
    $(".fale-conosco #select-produto").selectbox();
    $(".fale-conosco #select-motivo-esp").selectbox();
    $(".fale-conosco #select-estado").selectbox();
  }

if ($(".fale-conosco #select-motivo").length > 0){
  $(".fale-conosco #select-motivo").selectbox({
    onOpen : function (inst) {

      var containerSelect = $(this).parent(); //trg - dez2016
      var divSelectList = containerSelect.find('div[id^="sbHolder"]');

      divSelectList.each(function (){
        if (!$(this).hasClass('sbHolderDisabled')) {
          $(this).addClass('active');
          var listaOpts = $(this).find("ul.sbOptions");
          listaOpts.css({
            'border-top':'none',
            'border-color':'#8c3c8c',
            'top': $(this).position().top + 12
          });
        }
      });

      var optSelected = $(this).find('option:selected').val();
      var optListCustomSelect = containerSelect.find("ul li");

      //console.log('optSelected Open 1: ' + optSelected);
      //console.log(optListCustomSelect);

      optListCustomSelect.each(function (){
        if (($(this).text() == "Motivo") || ($(this).text() == "Especifique")) {
          $(this).hide();
        }
      });

      $('<span class="selectbox pull-right"></span>').appendTo(containerSelect.find("ul li:nth-child(2) a"));

    },
    onChange : function (inst,value) {

      var selectboxId = $(this).attr("id");
      var containerSelect = $(this).parent();
      var optSelected = inst;//containerSelect.find('a[id^="sbSelector"]').prop('rel');

      $(this).val(optSelected);

      if(selectboxId == 'select-motivo'){
    	  carregaSubMotivo(optSelected, document.getElementById('select-motivo-esp'));
    	  $(".fale-conosco #select-motivo-esp").selectbox("detach");

		  $(".fale-conosco #select-motivo-esp").selectbox({
				onOpen : function (inst) {

					var containerSelect = $(this).parent(); //trg - dez2016
					var divSelectList = containerSelect.find('div[id^="sbHolder"]');

					divSelectList.each(function (){
					  if (!$(this).hasClass('sbHolderDisabled')) {
  						$(this).addClass('active');
  						var listaOpts = $(this).find("ul.sbOptions");
  						listaOpts.css({
  						  'border-top':'none',
  						  'border-color':'#8c3c8c',
  						  'top': $(this).position().top + 12
  						});
					  }
					});

					var optSelected = $(this).find('option:selected').val();
					var optListCustomSelect = containerSelect.find("ul li");

					//console.log('optSelected Open 1: ' + optSelected);
					//console.log(optListCustomSelect);

					optListCustomSelect.each(function (){
					  if (($(this).text() == "Motivo") || ($(this).text() == "Especifique")) {
						$(this).hide();
					  }
					});

          var qtdItensLista = containerSelect.find('.sbOptions li').length - 1;

          if (qtdItensLista > 4) {
            containerSelect.find('.sbOptions').enscroll();
            containerSelect.find('.sbOptions').addClass('scrollbox');  
          }          

					$('<span class="selectbox pull-right"></span>').appendTo(containerSelect.find("ul li:nth-child(2) a"));

				},
				onChange : function (inst,value) {

					var selectboxId = $(this).attr("id");
					var containerSelect = $(this).parent();
					var optSelected = inst;//containerSelect.find('a[id^="sbSelector"]').prop('rel');

					$(this).val(optSelected);

					addTitleSelectedItem($(this),"Especifique");

				},
				onClose : function (inst) {

					$(this).find("a.sbHolder").css({
					  'border':'1px solid #ddd'
					});

					var divSelectList = $(this).parent().find('div[id^="sbHolder"]');

					divSelectList.each(function (){
					  if (!$(this).hasClass('sbHolderDisabled')) {
						$(this).removeClass('active');
					  }
					});

					var selectboxId = $(this).attr("id");
					var containerSelect = $(this).parent();
					var optSelected = inst.input.val();//containerSelect.find('a[id^="sbSelector"]').prop('rel');
					var optListCustomSelect = containerSelect.find("ul li");

					//console.log($(this).attr("id"));
					//console.log('optSelected Close 1: ' + optSelected);
					//console.log(optListCustomSelect);

					$('#' + selectboxId + ' option').each(function (){
					  if($(this).val() !== optSelected) { $(this).prop('selected', false); }
					});

				}
			});

      }

      addTitleSelectedItem($(this),"Motivo");

    },
    onClose : function (inst) {

      $(this).find("a.sbHolder").css({
        'border':'1px solid #ddd'
      });

      var divSelectList = $(this).parent().find('div[id^="sbHolder"]');

      divSelectList.each(function (){
        if (!$(this).hasClass('sbHolderDisabled')) {
          $(this).removeClass('active');
        }
      });

      var selectboxId = $(this).attr("id");
      var containerSelect = $(this).parent();
      var optSelected = inst.input.val();//containerSelect.find('a[id^="sbSelector"]').prop('rel');
      var optListCustomSelect = containerSelect.find("ul li");

      //console.log($(this).attr("id"));
      //console.log('optSelected Close 1: ' + optSelected);
      //console.log(optListCustomSelect);

      $('#' + selectboxId + ' option').each(function (){
        if($(this).val() !== optSelected) { $(this).prop('selected', false); }
      });

    }
  });
}
  //Disable after load
  if ($(".fale-conosco #select-produto").length > 0){
    $(".fale-conosco #select-produto").selectbox("disable");
    $(".fale-conosco #select-motivo-esp").selectbox("disable");
    $(".fale-conosco #select-estado").selectbox("disable");
  }

  addTitleSelectedItem($("#select-produto"),"Produto");
  addTitleSelectedItem($("#select-estado"),"Estado");

  //Custom hover
  $('.fale-conosco div[id^="sbHolder"]').hover(function(){

    $(this).parent().find('.sbHolder').addClass("sbHover");
    $(this).find('a[id^="sbToggle"]').addClass("sbHover");

    if (!$(this).find('ul[id^="sbOptions"]').is(":visible")) {
      $(this).find('a[id^="sbSelector"]').addClass("sbHover");
    }

  },function(){

    $(this).parent().find('.sbHolder').removeClass("sbHover");
    $(this).find('a[id^="sbToggle"]').removeClass("sbHover");
    if (!$(this).find('ul[id^="sbOptions"]').is(":visible")) {
      $(this).find('a[id^="sbSelector"]').removeClass("sbHover");
    }
  });

  //PRJ00014297 - c.s. - start
  $("#solicitacaoDebitoAutomatico .aviso.cliente-bb").hide();

  var multiplasFaturas = false;

  if ($("#solicitacaoDebitoAutomatico select[id$='banco']").length > 1) {
    multiplasFaturas = true;
  }

  $("#solicitacaoDebitoAutomatico select[id$='banco']").change(function() {

    if (multiplasFaturas) {

      if( $(this).find("option:selected").html() === "BANCO BRASIL"
          || $(this).find("option:selected").html() === "BANCO DO BRASIL"
          || $(this).find("option:selected").html() === "BRASIL" ) {
        $(this).parent().parent().parent().find(".aviso.cliente-bb").show();
      } else {
        $(this).parent().parent().parent().find(".aviso.cliente-bb").hide();
      }


    } else {
      if( $(this).find("option:selected").html() === "BANCO BRASIL"
          || $(this).find("option:selected").html() === "BANCO DO BRASIL"
          || $(this).find("option:selected").html() === "BRASIL" ) {
        $(this).parent().parent().find(".aviso.cliente-bb").show();
      } else {
        $(this).parent().parent().find(".aviso.cliente-bb").hide();
      }
    }

  });
  //PRJ00014297 - c.s. - end

}); // Fim Document Ready

function getParamsFromUrl(url){
    var search = url;
    var JsonParams = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
    return JsonParams;
}

function atrelaChangeAoSelectboxMesReferencia(){
  $(".mes-referencia ul a").each(function(){
    $(this).unbind();

	  var idFatura = $(this).attr('rel');
	  var url = window.location.href;

	  if(url.indexOf('#') >= 0)
		  url = url.substr(0, url.indexOf('#'));

	  if(url.indexOf("&FATURA=") >= 0){
		  $(this).attr('href', url.substr(0,url.indexOf("&FATURA=")+8)+idFatura);
	  } else {
		  $(this).attr('href', url+"&FATURA="+idFatura);
	  }
  });

}


// Method that verifies the size of combo box content and adapts the layout if necessary
function carregaSelectbox(value, titleValue){

  var tituloProduto;
  $("select[id^='select-produto-'] option").each( function () {

    produtoSelecionado = $(this).text();
    if (value != " "){
      produtoSelecionado = value;
      //console.log("\o/");
    }

    //console.log("Produto Selecionado: " + produtoSelecionado + ' ' + produtoSelecionado.length);

    for (var i = produtoSelecionado.length - 1; i >= 0; i--) {
      if (produtoSelecionado[i] === '(') {
        //console.log("Posicao " + i);
        tituloProduto = produtoSelecionado.slice( 0 , i );
        terminalProduto = produtoSelecionado.slice( i , produtoSelecionado.length );

        if((tituloProduto.length > 16) && (titleValue === true)){
          // Char limit for selected value
          tituloProdutoCutted = tituloProduto.substring(0,15) + '...';
          //console.log("A :)" + tituloProdutoCutted);
          tituloProduto = tituloProdutoCutted;

        } else if (tituloProduto.length > 25) {
          //Char limit for options list
          tituloProdutoCutted = tituloProduto.substring(0,24) + '...';
          //console.log("B :)" + tituloProdutoCutted);
          tituloProduto = tituloProdutoCutted;
        }
      }
    }
  });

  return tituloProduto;
}

function isSafari () {
  var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
  // At least Safari 3+: "[object HTMLElementConstructor]"
  return isSafari;
}

//4604 - Fale Conosco
function addTitleSelectedItem (seletor, label) {
  var containerSelect = seletor.parent();

  if (containerSelect.find('small').length === 0) {
    if(containerSelect.find('div').hasClass("sbHolderDisabled")){
      $('<small class="selected-item">' + label + ' <span class="glyphicon glyphicon-ok"></span></small>').appendTo(containerSelect);
    } else {
      $('<small class="selected-item">' + label + ' <span class="glyphicon glyphicon-ok green"></span></small>').appendTo(containerSelect);
    }
  }
}

function carregaSubMotivo(ddl1, ddl2) {
		var reclamacaoText = new Array('Reclamação sobre o *144/1057',
    								'Reclamação sobre o atendimento Loja',
    								'Pagamento em duplicidade',
    								'Reclamação de atendimento');
									
		var reclamacaoValue = new Array('reclamacao144',
    								'reclamacaoloja',
    								'pagamentoduplicidade',
    								'reclamacaoatendimento');
									
    	var duvidasText = new Array('Dificuldade para Navegação no Site',
    							 'Planos/Tarifas',
    							 'Patrocínio',
    							 'Bônus/Recarga');
		
		var duvidasValue = new Array('dificuldadenavegacao',
    							 'planotarifas',
    							 'patrocinio',
    							 'banusrecarga');

    	var sugestaoText = new Array('Sugestão');
		
		var sugestaoValue = new Array('sugestao');
		
    	var outrosText = new Array('Informações diversas',
    			 				'Informação de TP',
    			 				'Serviços',
    			 				'3G(Site)');

		var outrosValue = new Array('informacaodiversas',
    			 				'informacoestp',
    			 				'sercicos',
    			 				'3gsite');

    	var solicitacaoText = new Array('Reparo',
    								 '2º Via de Fatura',
    								 'Mudança de Endereço',
    								 'Alteração de Plano',
    								 'Alteração de Vencimento',
    								 'Alteração de Velocidade',
    								 'Alteração de Dados Cadastrais',
    								 'Bloqueio/Desbloqueio',
    								 'Cancelamento de Linha',
    								 'Contestação de Fatura',
    								 'Consulta a resposta da Contestação',
    								 'Envio de Chip',
    								 'Parcelamento',
    								 'Interatividade(cancelamento)',
    								 'Ativação de serviços',
    								 'Conta Paga não baixada');

		var solicitacaoValue = new Array('reparo',
    								 '2via',
    								 'mudancaendereco',
    								 'alteracaoplano',
    								 'alteracaovencimento',
    								 'alteracaovelocidade',
    								 'alteracaodadoscadastrais',
    								 'bloqueiodesbloqueio',
    								 'cancelamentolinha',
    								 'contestacaofatura',
    								 'consultarespostacontestacao',
    								 'enviochip',
    								 'parcelamento',
    								 'interatividade',
    								 'ativacaoservicos',
    								 'contapaganaobaixada');
	//Zera o selectbox
	ddl2.options.length = 0;
	createOption(ddl2,"Especifique","");

	switch (ddl1) {
			case 'reclamacao':
    			for (i = 0; i < reclamacaoText.length; i++) {
    				createOption(ddl2,reclamacaoText[i],reclamacaoValue[i]);
    			}
    			break;
    		case 'duvidas':
    			for (i = 0; i < duvidasText.length; i++) {
    				createOption(ddl2,duvidasText[i],duvidasValue[i]);
    			}
    			break;
    		case 'sugestao':
    			for (i = 0; i < sugestaoText.length; i++) {
    				createOption(ddl2,sugestaoText[i],sugestaoValue[i]);
    			}
    			break;
    		case 'outros':
    			for (i = 0; i < outrosText.length; i++) {
    				createOption(ddl2,outrosText[i],outrosValue[i]);
    			}
    			break;
    		case 'solicitacao':
    			for (i = 0; i < solicitacaoText.length; i++) {
    				createOption(ddl2,solicitacaoText[i],solicitacaoValue[i]);
    			}
    			break;
	}
	//ddl2.removeAttribute("disabled");
}

function createOption(ddl, text, value) {
	var opt = document.createElement('option');
	opt.value = value;
	opt.text = text;
	ddl.options.add(opt);
}

/*------------ 9227- START-Cadastro Usuario ---------------------*/
function callSelectBox (selectBoxesBases) {
   // body...
$(selectBoxesBases).selectbox({
 onOpen : function (inst) {

  var containerSelect = $(this).parent();
   var divSelectList = containerSelect.find('div[id^="sbHolder"]');

   divSelectList.each(function (){
    if (!$(this).hasClass('sbHolderDisabled')) {
      $(this).addClass('active');
      var listaOpts = $(this).find("ul.sbOptions");
      listaOpts.css({
        'border-top':'none',
        'border-color':'#C72177',
        'top': $(this).position().top + 12
      });
    }
  });

   var optSelected = $(this).find('option:selected').val();
   var optListCustomSelect = containerSelect.find("ul li");

      //console.log('optSelected Open 1: ' + optSelected);
      //console.log(optListCustomSelect);
      optListCustomSelect.each(function (){
        if (($(this).text() == "UF") || ($(this).text() == "Endereço") || ($(this).text() == "Complemento") || ($(this).text() == "Cidade")) {
          $(this).hide();
        }
      });

      $('<span class="selectbox pull-right"></span>').appendTo(containerSelect.find("ul li:nth-child(2) a"));


    },
    onChange : function (inst,value) {

      //var selectboxId = $(this).attr("id");
      //var containerSelect = $(this).parent().parent();
      //var optSelected = containerSelect.find('a[id^="sbSelector"]').html();
      //var optFirst = $(this).find('option[selected]').html();
      //console.log('Primeiro da Lista' + optFirst);

		var selectboxId = $(this).attr("id");
		var containerSelect = $(this).parent();
		var optSelected = inst;//containerSelect.find('a[id^="sbSelector"]').prop('rel');

		$(this).val(optSelected);

		addTitleItemCadastroUsuario($(this),getTitleById(selectboxId));

    },
    onClose : function (inst) {

      $(this).find("a.sbHolder").css({
        'border':'1px solid #ddd'
      });

      var divSelectList = $(this).parent().parent().find('div[id^="sbHolder"]');

      divSelectList.each(function (){
        if (!$(this).hasClass('sbHolderDisabled')) {
          $(this).removeClass('active');
        }
      });

	  var selectboxId = $(this).attr("id");
      var containerSelect = $(this).parent();
      var optSelected = inst.input.val();//containerSelect.find('a[id^="sbSelector"]').prop('rel');
      var optListCustomSelect = containerSelect.find("ul li");

      //console.log($(this).attr("id"));
      //console.log('optSelected Close 1: ' + optSelected);
      //console.log(optListCustomSelect);

      $('#' + selectboxId + ' option').each(function (){
        if($(this).val() !== optSelected) { $(this).prop('selected', false); }
      });

    }

  });
}

function getTitleById (id) {
	if('tipoDocumento' == id) {
		return 'Tipo Documento';
	}else if('endereco' == id){
		return 'Endereço';
	}else if('tipoComplemento1' == id || 'tipoComplemento2' == id || 'tipoComplemento3' == id){
		return 'Tipo Complemento';
	}
}

if ($(".dados-pessoais #cidade").length > 0){
  $(".dados-pessoais #cidade").selectbox("disable");
}

  //Custom hover Cadastro Usuario
  $('.dados-pessoais div[id^="sbHolder"]').hover(function(){

    $(this).parent().find('.sbHolder').addClass("sbHover");
    $(this).find('a[id^="sbToggle"]').addClass("sbHover");

    if (!$(this).find('ul[id^="sbOptions"]').is(":visible")) {
      $(this).find('a[id^="sbSelector"]').addClass("sbHover");
    }

  },function(){

    $(this).parent().find('.sbHolder').removeClass("sbHover");
    $(this).find('a[id^="sbToggle"]').removeClass("sbHover");
    if (!$(this).find('ul[id^="sbOptions"]').is(":visible")) {
      $(this).find('a[id^="sbSelector"]').removeClass("sbHover");
    }
  });

function  addTitleChangedItemCadastroUsuario (seletor, label) {
    var containerSelect = seletor.parent();

    if (containerSelect.find('small').length === 0) {
        $('<small class="selected-item">' + label + ' <span class=""></span></small>').appendTo(containerSelect);
    }
  }


  function addTitleSelectedItemCadastroUsuario (seletor, label) {
    var containerSelect = seletor.parent();

    if (containerSelect.find('small').length === 0) {
        $('<small class="selected-item">' + label + ' <span class="glyphicon glyphicon-ok"></span></small>').appendTo(containerSelect);
    }
  }

  function addTitleItemCadastroUsuario (seletor, label) {
    var containerSelect = seletor.parent();

    if (containerSelect.find('small').length === 0) {

      if(containerSelect.find('div').hasClass("sbHolderDisabled")){
        $('<small class="selected-item">' + label + ' <span class="glyphicon glyphicon-ok"></span></small>').appendTo(containerSelect);
      } else {
        $('<small class="selected-item" style="position: absolute; top: 12px;">' + label + ' <span class="glyphicon glyphicon-ok green"></span></small>').appendTo(containerSelect);
      }
    }
  }



  /*------------ 9227- END-Cadastro Usuario ---------------------*/
