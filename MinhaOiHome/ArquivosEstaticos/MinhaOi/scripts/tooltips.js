
/*
 * jQuery tooltip
 * http://jquery.com/
 *
 * Copyright 2011
 * Date: Tue Jul 18 2011 0200
 *
 */


$(function(){

  	criaTooltipGeneric2();
	atribuiTooltipEnd();
	graficoSaldoDadosReal();
	reloadSelectboxPosition();

	tooltipConsumo3g();
	tooltipDescricaoConsumo3g();

});


function criaTooltipGeneric2(){

	// Tooltip big original com classes modificadas
    $('.tooltipLinkLarge2').each(function(){
        if ($(this).attr("tooltip")) {
            if(!$(this).next('a.tooltipLinkLarge2').length) $(this).after('<div class="tooltipAnimate">' + $(this).attr("tooltip") + '</div>')
        }
    })


    $("a.tooltipLinkLarge2").hover(function(){
        position = $(this).position();
        $(this).next("div.tooltipAnimate").css({
            top: position.top - 105,
            left: position.left - 60,
            opacity: 0,
            display: 'block'
        }).stop(true, true).animate({
            opacity: 1,
            left: position.left - 60
        }, "slow", function(){
            $(this).show();
        });
    }, function(){
        $(this).next("div.tooltipAnimate").stop(true, true).animate({
            opacity: 0,
            top: $(this).position().top - 105
        }, "fast", function(){
            $(this).hide();
        });
    });

}

//Tooltip para a funcionalidade Endereco de Cobranca
function atribuiTooltipEnd(){ //l.c
	//Tooltip Alteracao de Endereco 2011
	$('.popupAlterEnd').hide();
	$('.inputDtAlt').mouseover(function() {
		$('div.popupAlterEnd').hide();
		$('div.popupAlterEnd').fadeIn("fast");
	});

	$('.inputDtAlt').mouseleave(function() {
		$('div.popupAlterEnd').fadeOut("fast");
	});

	//Alterado por condicao do selectbox e melhorias - l.c - 07/01/2013
	var $divSelect = $('.inputDtAlt');
	var $divTooltip = $('.popupAlterEnd');
	var $select = $("#dt-venc");

	$divTooltip.hide();
	$select.change(function(){

			var $value = $select.val();
			var $spanVenc = $("#spanDueDate");
			var $spanFecha = $("#spanCutOff");
			var $iptDateV = $("#hiddenSelectedDueDate");
			var $iptCycle = $("#hiddenSelectedCycle");
			var $iptOff = $("#hiddenSelectedCutOff");

			if($value != null && $value != ""){

				var arr = $value.split("|");
				if(arr != null && arr.length == 3){
					$spanVenc.html(arr[0]);
					$spanFecha.html(arr[1]);
					$iptDateV.val(arr[0]);
					$iptOff.val(arr[1]);
					$iptCycle.val(arr[2]);
				}
				$divSelect.mouseover(function() {
					$divTooltip.show();
				});

				$divSelect.mouseleave(function() {
					$divTooltip.hide();
				});
			}else{

				$spanVenc.html("");
				$spanFecha.html("");
				$iptDateV.val("");
				$iptOff.val("");
				$divSelect.mouseover(function() {
					$divTooltip.hide();
				});

				$divSelect.mouseleave(function() {
					$divTooltip.hide();
				});

			}
	});
}

//Funcao Grafico dinamico Saldo Dados para consumo real de internet - l.c
//Funcao migrada para tooltip para evitar recarregamento excessivo -  alterado na release de 21/08/2012 Saldo 3G - l.c
//Funcao alterada para inclusao de pacote complementar de dados - 14/05/2014 55460 Redirect - c.s.
function graficoSaldoDadosReal(){
 console.log("##########graficoSaldoDadosReal################");
	if(navigator.userAgent.toLowerCase().indexOf('chrome') != -1){

		$('#saldoDados.account-saldoAtual tfoot.chrome.pacoteComplementar td.two').css({
			'width':'172px'
		});

	}
	var counter=0;
	$('.item-plano.pacote').each(function(){
		$(this).attr('id', 'counter'+counter);
		counter++;
	});
	
	$('.consumo3g li.percentSaldo .percetConsumido').each(function(){

		//Pacote Mensal
		var valorConsumido = parseFloat($('.consumo3g li.percentSaldo .traco b').html());
		
		if (isNaN(valorConsumido)){
			valorConsumido=0;
		}
		
		var exValorConsumido = parseFloat($('.consumo3g li.valConsumido b').html());
		var maxValorConsumido = $('.consumo3g li.first.text-right b').html();
		
		var maxValorConsumidoVal = maxValorConsumido;
		var unidadeValorConsumido;
			
		if (maxValorConsumido.length == 0) {
			maxValorConsumido = 0;
		} else {
			maxValorConsumido = maxValorConsumido.split('&nbsp;')[0];
			unidadeValorConsumido = maxValorConsumidoVal.split('&nbsp;')[1];
			
		}
		
		//Pacote Complementar
		var valorConsumidoComp = parseFloat($('.consumo3g.consumoComplementar li.percentSaldo .traco b').html());
		if (isNaN(valorConsumidoComp)){
			valorConsumidoComp=0;
		}
		
		var exValorConsumidoComp = parseFloat($('.consumo3g.consumoComplementar li.valConsumido b').html());
		var maxValorConsumidoComp = $('.consumo3g.consumoComplementar li.first.text-right b').html();
		var maxValor = maxValorConsumidoComp;
		var unidadeValorConsumidoComp;
		if (($('div.consumo3g.consumoComplementar').length) == 0) {			
			maxValorConsumidoComp = 0;
		} else {						
			maxValorConsumidoComp = maxValorConsumidoComp.split('&nbsp;')[0];
			unidadeValorConsumidoComp = maxValor.split('&nbsp;')[1];
		}
		
		
		if (isNaN(valorConsumidoComp)) {
			valorConsumidoComp = parseFloat($('.consumo3g.consumoComplementar li.percentSaldo .consumoZero b').html());
		}
		
		//Defect 27
		var $consumo3gpercet = $(this).find('.percetConsumido');
		var $consumo3gpontilhado = $(this).find('.traco');
		var $consumo3glinhaConsumido = $(this).find('li.valConsumido');

		//Pacote Mensal
        var mioloConsumoImg = valorConsumido * parseFloat(3.3);
    	largConsumo = 25 + mioloConsumoImg;

	    //Pacote Complementar
	    var mioloConsumoImgComp = valorConsumidoComp * parseFloat(3.3);
    	largConsumoComp = 25 + mioloConsumoImgComp;

		// Verifica se usuario possui pacote complementar de dados
	    var possuiPacoteComplementar = $('#saldoDados.account-saldoAtual div').hasClass('pacoteComplementar');

		if(valorConsumido == 0){
			$('#saldoDados').css('padding-bottom','20px');
		
			if((exValorConsumido==0 || exValorConsumido=='0') && (maxValorConsumido==0 || maxValorConsumido=='0')){
				document.getElementById('counter0').style.visibility = "collapse";
				$('#counter0').css('display','none');
			}else {
				// Cenario 1
				$('.consumo3g .saldoFranquiaTotal').hide();
				$('.consumo3g li.percentSaldo .leftConsumido').hide();
				$('.consumo3g li.valConsumido').hide();
				$('.consumo3g li.percentSaldo .percetConsumido').show();
					$(this).parent().removeClass('On');
					$(this).removeClass('parcial');
					$('.consumo3g li.percentSaldo .percetConsumido .traco b').html(valorConsumido);// 55460 - c.s 14/05/2014
					$('.consumo3g li.valConsumido').css('margin-left',(largConsumo - 25));
					$('.consumo3g li.percentSaldo .percetConsumido div').removeClass('traco');
					$('.consumo3g li.percentSaldo .percetConsumido div').addClass('consumoZero');
					$('.consumo3g li.percentSaldo .percetConsumido').css('width','0px');
					$('.consumo3g li.percentSaldo .percetConsumido').addClass('clear');
			}

		} else if((valorConsumido >=1 ) && (valorConsumido < 100)) {
		 	$('#saldoDados').css('padding-bottom','20px');
			
			// Cenario 1
	        $('.consumo3g .saldoFranquiaTotal').hide();
	        $('.consumo3g li.percentSaldo .leftConsumido').show();
	        $('.consumo3g li.percentSaldo .percetConsumido').show();
	        $('.consumo3g li.valConsumido').show();
	        	$(this).parent().removeClass('On');
		        $(this).addClass('parcial');
		        $(this).css('width',largConsumo);
		        $('.consumo3g li.percentSaldo .percetConsumido .traco').css('width','100px');
		        $('.consumo3g li.valConsumido').css('margin-left',(largConsumo - 25));
				if ($('.consumo3g li.valConsumido div b').text().length == 4) {
					$('.consumo3g li.valConsumido div b').css('margin-left','5px');
				} else if ($('.consumo3g li.valConsumido div b').text().length == 3) {
					$('.consumo3g li.valConsumido div b').css('margin-left','7.5px');
				} else if($('.consumo3g li.valConsumido div b').text().length == 2) {
					$('.consumo3g li.valConsumido div b').css('margin-left','11px');
				} else if($('.consumo3g li.valConsumido div b').text().length == 1) {
					$('.consumo3g li.valConsumido div b').css('margin-left','15px');
				}

		        $('.consumo3g li.percentSaldo .percetConsumido .traco b').html(valorConsumido);
				$('.consumo3g.consumoComplementar li.percentSaldo .traco b').html(valorConsumidoComp);
				if($('#terminalSelecionado').length) {
					$('.consumo3g li.valConsumido span').css('top', '140px');
				} else {
					$('.consumo3g li.valConsumido span').css('top', '85px');
				}
		} else if(valorConsumido == 100){
			$('#saldoDados').css('padding-bottom','20px');

			// Cenario 3
			if(exValorConsumido <= maxValorConsumido) {
	        $('.consumo3g li.valConsumido').hide();
	        $('.consumo3g .leftConsumido').hide();
	        $('.consumo3g .percetConsumido').hide();
	        	$(this).removeClass('parcial');
		        $(this).parent().addClass('On');
		        $('.consumo3g .saldoFranquiaTotal').show();
			
			}else if (exValorConsumido > maxValorConsumido) {
			
				if($('#warnExceedOne').length <= 0) {
				
					var exValorTotalConsumido = (exValorConsumido * 100) / maxValorConsumido;
					exValorTotalConsumido = String(exValorTotalConsumido.toFixed(2));
					exValorTotalConsumido = exValorTotalConsumido.concat("%");
									
					$('.consumo3g .leftConsumido').hide();
					$('.consumo3g .percetConsumido').hide();
					$('.consumo3g li.percentSaldo .saldoFranquiaTotal').show();
					$('.consumo3g li.percentSaldo .saldoFranquiaTotal strong').css('padding-left', '5px');
					$('.consumo3g li.percentSaldo .saldoFranquiaTotal strong').html(exValorTotalConsumido);
					
					$('.consumo3g li.valConsumido').css({ marginRight: 10 , color: '#ff0000' , 'float': 'right'})
					.after("<div id='warnExceedOne' class='warnExceed extraInfo' style='display: none'><ul><li class='topo'></li><li class='miolo'><p><b class='bLonger red'><span id='consumoExcedenteOne' class='tot-exced'> </span> excedentes </b>do total de <b id='consumoFranquiaOne' class='bLonger tot-franq'></b> da franquia contratada. </p></li><li class='base'></li></ul></div>");
					$('.boxRealConnsumo').css('padding-top', '85px');
					
					var valorExcedenteConsumido =  exValorConsumido - maxValorConsumido;
					if(valorExcedenteConsumido % 1 != 0) {
						valorExcedenteConsumido = valorExcedenteConsumido.toFixed(2);
					}
					valorExcedenteConsumido = String(valorExcedenteConsumido);
					valorExcedenteConsumido = valorExcedenteConsumido.concat(" ",unidadeValorConsumido);						
					maxValorConsumido = String(maxValorConsumido);
					maxValorConsumido = maxValorConsumido.concat(" ",unidadeValorConsumido);
					
					// SE A  UNIDADE FOR REAIS, DIVIDIR O EXCEDENTE POR 100
					if(unidadeValorConsumido == 'REAIS'){

						valorExcedenteConsumido = valorExcedenteConsumido.concat("");
						if(valorExcedenteConsumido.length > 2){
							centavos = valorExcedenteConsumido.substring(valorExcedenteConsumido.length -2, valorExcedenteConsumido.length);
							reais = valorExcedenteConsumido.substring(0, valorExcedenteConsumido.length -2);
							valorExcedenteConsumido = reais.concat(",");
							valorExcedenteConsumido = valorExcedenteConsumido.concat(reais);
						}
					}
					
					document.getElementById("consumoExcedenteOne").innerHTML = valorExcedenteConsumido;
					document.getElementById("consumoFranquiaOne").innerHTML = maxValorConsumido;
					
				};		
			};
		}
		
		if (possuiPacoteComplementar) {
		
			if(valorConsumidoComp == 0) {
		 		$('#saldoDados' ).css('padding-bottom','5px');
		 		//Cenario 2
				$('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido').css('width', '0px');
				$('.consumo3g.consumoComplementar .saldoFranquiaTotal').hide();
		        $('.consumo3g.consumoComplementar li.percentSaldo .leftConsumido').hide();
		        $('.consumo3g.consumoComplementar li.valConsumido').hide();
		        $('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido').show();
		        	$(this).parent().removeClass('On');
			        $('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido').removeClass('parcial');
			        $('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido .traco b').html(valorConsumidoComp); // 55460 - c.s 14/05/2014
			        $('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido div').removeClass('traco');
			        $('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido div').addClass('consumoZero');
				if ($('.consumo3g.consumoComplementar li.valConsumido div b').text().length == 4) {
					$('.consumo3g.consumoComplementar li.valConsumido div b').css('margin-left','5px');
				} else if ($('.consumo3g.consumoComplementar li.valConsumido div b').text().length == 3) {
					$('.consumo3g.consumoComplementar li.valConsumido div b').css('margin-left','7.5px');
				} else if($('.consumo3g.consumoComplementar li.valConsumido div b').text().length == 2) {
					$('.consumo3g.consumoComplementar li.valConsumido div b').css('margin-left','11px');
				} else if($('.consumo3g.consumoComplementar li.valConsumido div b').text().length == 1) {
					$('.consumo3g.consumoComplementar li.valConsumido div b').css('margin-left','15px');
				}
			} else if ((valorConsumidoComp >=1 ) && (valorConsumidoComp < 100)) {

				//Cenario 4
				$('.consumo3g.consumoComplementar .saldoFranquiaTotal').hide();
				$('.consumo3g.consumoComplementar li.percentSaldo .leftConsumido').show();
				$('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido').show();
				$('.consumo3g.consumoComplementar li.valConsumido').show();
					$(this).parent().removeClass('On');
					$(this).addClass('parcial');
					$('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido').css('width',largConsumoComp);
					$('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido div').removeClass('consumoZero');
					$('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido div').addClass('traco');
					$('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido .traco').css('width',largConsumoComp);
					$('.consumo3g.consumoComplementar li.valConsumido').css('margin-left',(largConsumoComp - 25));
					if($('#terminalSelecionado').length) {
						$('.pacoteComplementarOn li.valConsumido span').css('top', '390px');
					} else {
						$('.pacoteComplementarOn li.valConsumido span').css('top', '335px');
					}
					$('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido .traco b').html(valorConsumidoComp);
					if ($('.consumo3g.consumoComplementar li.valConsumido div b').text().length == 4) {
						$('.consumo3g.consumoComplementar li.valConsumido div b').css('margin-left','5px');
					} else if ($('.consumo3g.consumoComplementar li.valConsumido div b').text().length == 3) {
						$('.consumo3g.consumoComplementar li.valConsumido div b').css('margin-left','7.5px');
					} else if($('.consumo3g.consumoComplementar li.valConsumido div b').text().length == 2) {
						$('.consumo3g.consumoComplementar li.valConsumido div b').css('margin-left','11px');
					} else if($('.consumo3g.consumoComplementar li.valConsumido div b').text().length == 1) {
						$('.consumo3g.consumoComplementar li.valConsumido div b').css('margin-left','15px');
					}

			} else if(valorConsumidoComp == 100){				
					
					//Cenario 5
					if(exValorConsumidoComp <= maxValorConsumidoComp) {
					
						$('.consumo3g.consumoComplementar li.valConsumido').hide();
						$('.consumo3g.consumoComplementar .leftConsumido').hide();
						$('.consumo3g.consumoComplementar .percetConsumido').hide();
						$('.consumo3g.consumoComplementar li.percentSaldo .saldoFranquiaTotal').show();
					
					}else if (exValorConsumidoComp > maxValorConsumidoComp) {
						
						var exValorTotal = (exValorConsumidoComp * 100) / maxValorConsumidoComp;
						exValorTotal = String(exValorTotal.toFixed(1));						
						exValorTotal = exValorTotal.concat("%");
						$('.consumo3g.consumoComplementar .leftConsumido').hide();
						$('.consumo3g.consumoComplementar .percetConsumido').hide();
						$('.consumo3g.consumoComplementar li.percentSaldo .saldoFranquiaTotal').show();
						$('.consumo3g.consumoComplementar li.percentSaldo .saldoFranquiaTotal strong').css('padding-left', '5px');
						$('.consumo3g.consumoComplementar li.percentSaldo .saldoFranquiaTotal strong').html(exValorTotal);
						if($('#warnExceed').length <= 0) {
						
							//Para saldo que exceder a fatura
							
							$('.consumo3g.consumoComplementar .percetConsumido').parent().addClass('On exceeded');
							/* 8180 / 8182 / 8184 */
							$('.consumo3g.consumoComplementar li.percentSaldo .percetConsumido .traco').css({width: '100%' , background: 'none'})//alinhamento para quando o valor for 0
									   .find('b').html(valorConsumidoComp);							
							
							/* 8180 / 8182 / 8184 */
							$('.consumo3g.consumoComplementar li.valConsumido').addClass('secondBar');
							$('.consumo3g.consumoComplementar li.valConsumido.secondBar').css({ marginRight: 10 , color: '#ff0000' , 'float': 'right'})
							.after("<div id='warnExceed' class='warnExceed extraInfo' style='display: none'><ul><li class='topo'></li><li class='miolo'><p><b class='bLonger red'><span id='consumoExcedente' class='tot-exced'> </span> excedentes </b>do total de <b id='consumoFranquia' class='bLonger tot-franq'></b> da franquia contratada. </p></li><li class='base'></li></ul></div>");
							$('.boxRealConnsumo.consumoComplementar').css('padding-top', '85px');
							
							var valorExcedente =  exValorConsumidoComp - maxValorConsumidoComp;
							if(valorExcedente % 1 != 0) {
								valorExcedente = valorExcedente.toFixed(2);
							}
							valorExcedente = String(valorExcedente);
							valorExcedente = valorExcedente.concat(" ",unidadeValorConsumidoComp);						
							maxValorConsumidoComp = String(maxValorConsumidoComp);
							maxValorConsumidoComp = maxValorConsumidoComp.concat(" ",unidadeValorConsumidoComp);
							// SE A  UNIDADE FOR REAIS, DIVIDIR O EXCEDENTE POR 100
							if(unidadeValorConsumidoComp == 'REAIS'){

								valorExcedente = valorExcedente.concat("");
								if(valorExcedente.length > 2){
									centavos = valorExcedente.substring(valorExcedente.length -2, valorExcedente.length);
									reais = valorExcedente.substring(0, valorExcedente.length -2);
									valorExcedente = reais.concat(",");
									valorExcedente = valorExcedente.concat(reais);
								}
							}
							
							document.getElementById("consumoExcedente").innerHTML = valorExcedente;
							document.getElementById("consumoFranquia").innerHTML = maxValorConsumidoComp;
							
						};
					};
				};
			};
		
		// 55460 to align layout with prototype
		$('.valConsumido span').css('width', '60px');

		reloadSelectboxPosition();
	});
}

// Tooltip para a funcionalidade Consulta Saldo (Saldo Parcial - Arbor)
function tootltipDescricaoFranquiaSaldoNovo(){ //e.m 29/08/2013


	$toolSaldoInfoDataBase = $(".saldo-parcial .tool1Alerta");
	$toolSaldoInfoDataBase.each(function(){
		//seletores
		$textTool = $(this).find(".popupAlertaSms");
		$tooltBuilt = $(this).find('div:eq(1)');
		$icoTool = $(this).find(".tooltipIco");

		if($textTool != ''){
			text1 = $textTool.find("p").html().substring(0,96);
			text_compl = $textTool.find("p").html().substring(96);

			//Montagem
			$textTool.hide();
			$tooltBuilt.hide();
			$(this).find(".tip-title1").html(text1);
			$(this).find(".tip-title").html(text_compl);

			//Efeito
			$icoTool.mouseenter(function() {
				var posX = $(this).position().top;
				var posY = $(this).position().left;
				$(this).next().next()
						.css({'top':posX-45+'px','left':posY+18+'px'})
						.fadeIn("fast");
			});
			$icoTool.mouseleave(function() {
				$(this).next().next().fadeOut("fast");
			});
		}
	});
}

function tootltipBarraConsumoSaldoNovo(){ //e.m 29/08/2013

	// Tool tip for bar graph
	// #parcial .barraRealConsumo
	$grapicoTool = $('a[id^="barra-grafico_"]');

	$grapicoTool.each(function(){

		$grapicoIcoTool = $(this).find(".text-tooltip");

		var posX = $(this).position().top;
		var posY = $(this).position().left;

		$toolTipMessage = $(this).find(".barToolTip");
		$toolTipMessage.addClass('tip-title');

		$(this).mouseenter(function(){
			$('.text-tooltip').css('width', '200px');
			$('.text-tooltip').css('height', '100px');

			$grapicoIcoTool.css({'top':posX-105+'px','left':posY+107+'px'})
			.fadeIn("fast");
		});

		$('.tool1Alerta').mouseleave(function() {
			$('.text-tooltip').hide();
			$grapicoIcoTool.fadeOut(900);
		});

	});
}

// Tooltip para a funcionalidade Consumo 3G (Saldo Dados - IN)
function tooltipConsumo3g () {

	$toolSaldoInfoDataBase = $(".saldo-dados .tool1Alerta");
	$toolSaldoInfoDataBase.each(function(){
		//seletores
		$textTool = $(this).find(".popupAlertaEmail");
		$tooltBuilt = $(this).find('div:eq(1)');
		$icoTool = $(this).find(".tooltipIco");

		if($textTool != ''){
			//text1 = $textTool.find("p").html().substring(0,96);
			//text_compl = $textTool.find("p").html().substring(96);
			text1 = $textTool.find("p").html();
			text_compl = $textTool.find("p").html();

			//Montagem
			$textTool.hide();
			$tooltBuilt.hide();
			$(this).find(".tip-title1").html(text1);
			$(this).find(".tip-title").html(text_compl);

			//Efeito
			$icoTool.mouseenter(function() {
				var posX = $(this).position().top;
				var posY = $(this).position().left;
				$(this).next().next()
						.css({'top':posX-45+'px','left':posY+18+'px'})
						.fadeIn("fast");
			});
			$icoTool.mouseleave(function() {
				$(this).next().next().fadeOut("fast");
			});
		}
	});

}

// Tooltip para a funcionalidade Consumo 3G (Saldo Dados - IN)
function tooltipDescricaoConsumo3g () {
	//Novo script saldo 3g, sem conflitos - jan/2013 - l.c
	//anterior $tool = $(".tool1Alerta");
	//favor nao reutilizar esse conjunto [id, class, class] ago/2013 - e.m
	$tool = $(".saldo-dados .tool1Alerta");
	//$tool = seletor;

	$tool.each(function(){
		//seletores
		$textTool = $(this).find("div:first");
		$tooltBuilt = $(this).find('div:eq(1)');
		$icoTool = $(this).find(".tooltipIco");
		text1 = $textTool.find("p").html().substring(0,98); //Teste 55460 - c.s.
		text_compl = $textTool.find("p").html().substring(99); //Teste 55460 - c.s.

		//Montagem
		$textTool.hide();
		$tooltBuilt.hide();

		if(text_compl != null && text_compl != ""){
			$(this).find(".tip-title").html(text_compl);
		}

		if(text1 != null && text1 != ""){
			$(this).find(".tip-title1").html(text1);
		}

		//Efeito
		$icoTool.mouseenter(function() {
			var posX = $(this).position().top;
			var posY = $(this).position().left;
			$(this).next().next()
					.css({'top':posX-45+'px','left':posY+18+'px'})
					.fadeIn("fast");
		});
		$icoTool.mouseleave(function() {
			$(this).next().next().fadeOut("fast");
		});
	});

}