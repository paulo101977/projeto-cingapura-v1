//Inclusao usuario - integracao de portais
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};
//Fim inclusao usuario

$(document).ready(function(){
	ieFixes();
    enablePlaceholder();
    sweetForm();
    formInlineEdit();
    userActions();
    linksAjax();
    manageFaturaDetalhada();
    setasVencimentoAnaliseConta();
    trataBotao();
	createTooltipIcos();
    collapseExpand();
	atribuiTabs();
	paymentVazio();
	//graficoSaldoDadosReal();
	//tabsMiolo();//SoluÃ§Ã£o provisÃ³ria 55772 QA - leo.carvalho
	boxDaccCtOnline();
	// Se o checkbox todos estiver marcado ele desabilita as outras opcoes
	$("input[type=checkbox]#todos").click(function(){
		if($("input[type=checkbox]#todos:checked").length == 1) {
			$(".radiogroup2 input:not(#todos)").attr({"disabled" : "disabled", "checked" : false});
		} else {
			$(".radiogroup2 input:not(#todos)").attr("disabled", false);
		}
	});
	
	$("section.account-calendar div.calendar:nth-child(3)").addClass("nth-child3");
	$('#data1, #data2').focus(function(){
		if (this.value == 'DD/MM/AAAA') {
			this.value = ''
			$(this).setMask({
				attr: "alt"
			})
		}
	}).blur(function(){
		if(this.value=='')this.value='DD/MM/AAAA'
	})
	

    $('.tablesInfo').after('<span class="bottomtablesInfo" />');
    $('h3:first-child, .tablesInfo li:first-child, .tablesInfo tr:first-child td:first-child, tr:first-child, th:first-child, td:first-child').addClass('first-child');
    $('h3:last-child, .tablesInfo li:last-child, th:last-child, td:last-child, tr:last-child').addClass('last-child')
    $('.sweetTable tr:last td').css('background','none')


    $(".ajaxtable").click(function(){ 
	
		if($.fancybox) $.fancybox('<div class="modal-loading"><img src="images/ajax-loader.gif" class="fleft" alt="Carregando" /><p class="fleft">CARREGANDO</p></div>' ,{
			showCloseButton: false, padding: 0, centerOnScroll: true
		})

		$('.modal-loading').parent().parent().parent().addClass('modal-loadingparent')

        jQuery.get('tela_tabela.html', function(data){
			i=setTimeout('fecharfancy()', 1000)
   			$("div#content").html(data)
			//trataConteudo('body')

		    //if (!$.browser.msie) { 04/08/2014
		    if (!(window.navigator.userAgent.indexOf() == -1)) {		    	
		        $('.data-tabela').addClass('roundedBox')
		    }


            $('a.topen').toggle(function(){
                $(this).text("+ Detalhes").addClass('tclose').removeClass('topen').next('div.acco').show()
                $(this).next('div.acco').hide()
            }, function(){
                $(this).text("- Ocultar").attr('title','Ocultar').addClass('topen').removeClass('tclose').next('div.acco').show()
            })
            ieFixes()

		    //if ($.browser.msie) { 04/08/2014
		    if ((window.navigator.userAgent.indexOf() == -1)) {
		        $('.data-tabela').before('<div class="data-tabela-top" />');
		        $('.data-tabela').after('<div class="data-tabela-bottom" />');
		    }
        })

		return false
		
    })
	
	// Funcao para exibir e esconder a Detalhe Caixa postal
	var dlCaixaPostal = $('#detailsCaixaPostal');
	dlCaixaPostal.find('dd').hide().end().find('.maisDetalhes').click(function(e) {
		
		var isto = $(this); // Variavel criada para evitar a continua busca do 'objeto' dlCaixaPostal
		var lblIsto = isto.text(); // Variavel de texto do 'objeto' isto
		
		e.preventDefault();
		isto.parents('dt').next('dd').slideToggle();
		
		// Condicao criada para verificar o texto do link .maisDetalhes e alterar conforme o click
		if (lblIsto == '+ Detalhes') {
			isto.text('- Ocultar').attr('title','Ocultar');
		} else {
			isto.text('+ Detalhes').attr('title','Detalhes');
		}
   });
   dlCaixaPostal.find('dd.open').slideToggle();

    
    //if (!$.browser.msie) { 04/08/2014
	if (!(window.navigator.userAgent.indexOf() == -1)) {
        $('.data-tabela').addClass('roundedBox')
    }
    

	// Apenas para IE6
	//if (jQuery.browser.msie && jQuery.browser.version == '6.0') {
	//  $('.borderbox').remove();
	//}
  
	if($().checkbox) $('input.iphoneLikeCheckbox:checkbox:not([safari])').checkbox();  
    
  $('.bt-close').on('click', function(){
    $(this).parent().slideUp('normal', accountTabsIEFix);
    return false;
  });

	$('.submit, .reset').on('mouseenter', function () {
		$(this).addClass('bthover');
	}).on('mouseleave', function () {
	  $(this).removeClass('bthover');
	});
    
    $('#content h2 + p').addClass('pafterh2')
    $('.bottomtablesInfo + h2').addClass('h2afterborder')
    $('#content h2.main-title + p').removeClass('pafterh2')
    $('.legaltext').wrap('<div class="legaltextwrapper" />')
    $('.boxersidebyside').append('<span class="boxsidebysidebottom" />')
    
    
    // Procura o pai da LI que est&aacute; com CHECKGREEN e adiciona classe
    $('li.checkgreen').parent().addClass('checkparent');
    $('.boxer').before('<span class="topbox" />');
    $('.boxprotocol').append('<span class="borderbottomboxprotocol" />');
    $('.account-box.messageItem').append('<span class="account-message" />');
    
    $('div.selectbox-wrapper').wrap('<div class="selectwrapperwrap" />');
    
    // Altura do div que faz a pagina de login
    var alturabody = $(window).height();
    $('#login .pageLogin').append('<span class="tarjaloginBottom"></span>')
    $('#login .pageLogin').prepend('<span class="tarjaloginTop"></span>')
    
		$(".sbanco .selectbox-wrapper li").on("click", function(){
			if ($(this).parents(".selectbox-wrapper").parent().find("select option.cbanco").is(':selected')) {
				$(this).parents(".sbanco").parent().find(".cfbanco").show();
			}
			else {
				$(this).parents(".sbanco").parent().find(".cfbanco").hide();
			}
    });
    
   // Ativa ou nao painel para insercao de novo endereco de instalacao
    $(".contEndDifInstalacao").hide()
	$('input[name|="endInstalacao"]').change(function() {
		if($(this).val()=='Nao') {
			$('.contEndDifInstalacao').show();
		}  else {
			$('.contEndDifInstalacao').hide();
			$('#enderecoNovoCep').val('');
			$('#enderecoNovoRuaValid').val('');
			$('#enderecoNovoRuaValid_input').val('');
			$('#enderecoNovoNumero').val('');
			$('#enderecoNovoComplemento').val('');
			$('#enderecoNovoBairro').val('');
			$('#enderecoNovoCidade').val('');
			$('#enderecoNovoEstado').val('');
    }
	});
    
	addInputBorders();
    
    $('.panel-vertical .accountBox').each(function(){
        if (!($(this).find('.account-message').length)) {
            $(this).append('<div class="account-footerPanel"> </div>')
        }
    })

    $('form input[type="text"], form input[type="password"]').each(function(){
        if ($(this).attr("inputInfo")) {
            $(this).after('<div class="inputInfo">' + $(this).attr("inputInfo") + '</div>')
        }
    })
    $("form input[inputInfo]").focus(function(){
        $(this).parent().find("div.inputInfo").css({
            left: $(this).width() + $(this).position().left + 25
        }).animate({
            opacity: "show",
            left: $(this).width() + $(this).position().left + 15
        }, "slow");
    }).blur(function(){
        $(this).parent().find("div.inputInfo").animate({
            opacity: "hide",
            left: $(this).width() + $(this).position().left + 45
        }, "fast");
    });

    $('table.oiAgenda a.oiAgenda-editar').on('click', function(){
        return false
    })

    atribuiFancyboxes();

    $('ul.ul-expandCollapse').each(function(){
        $(this).wrap('<div class="ul-expandCollapse clearfix" />')
        $(this).find('li').hide()
        $(this).find('li').eq(0).show();
        $(this).after('<a href="#" class="ul-expandCollapse" title="Detalhes">+ Detalhes</a>')
        reloadSelectboxPosition()
    });

    $('a.ul-expandCollapse').toggle(function() {
        $(this).text("- Ocultar").attr('title','Ocultar').parents('div.ul-expandCollapse').addClass('ul-opened').find('li').show();
        reloadSelectboxPosition()
    }, function() {
        $(this).text("+ Detalhes").attr('title','Detalhes').parents('div.ul-expandCollapse').removeClass('ul-opened').find('li').hide();
        $(this).prev('ul').find('li').eq(0).show();
        reloadSelectboxPosition()
    });

	$('#criacaoLoginSenha .regulamento div').load("/ArquivosEstaticos/MinhaOi/Regulamento/Termos_Condicoes_Cadastro.htm");
	$('#dadosTerceiroCommand .regulamento div').load("/ArquivosEstaticos/MinhaOi/Regulamento/Termos_Condicoes_Cadastro.htm");
	//inclusao usuario - cookie portais
	try{
	//var nomeCliMinhaOi = document.getElementsByClassName("nameUser")[0].innerHTML;
	jQuery(document).ready(function () {
	var nomeCliMinhaOi = jQuery(".nameUser").text();
	jQuery.cookie("nomeCliMinhaOi", nomeCliMinhaOi, { expires: 365, domain: 'oi.com.br'});
	});
	}catch(e){}

	
	//corrigi box do tooltip para fincionalidades que exibem imgs de exemplo.
	$('#crieSeuLogin .codigoSeguranca div.tooltip img').parent().css({

	    	background: 'none'
	});
	//FIM - inclusao usuario cookie
	
	// 51951 - correÃ§Ã£o para valor mÃ­nimo Template fixo
	$('#val-inf').each(function() {
		$(this).parent().parent('.account-overview').addClass('val-minimo');
		
	})
	
	//para prd provisorio - captcha meus produtos
	$('#content #validaTokenFixo #captchaImg').parent().css('margin-top','-80px');
	$('#content #validaTokenFixo input.reset').prev().css('margin','10px 0');
	
	/*57671 - Chat SSO*/
    $('.area-sso #geral.cadastro').each(function(){
    	if($('.area-sso .cadastro .info-sso').is(':visible')){
	    	$(this).find(".info-sso").after('<section id="chat-SSO"></section>');
	    	
    	}else if($('.area-sso #geral.cadastro aside').is(':visible')){
	    	$(this).find("aside").prev().after('<section id="chat-SSO"></section>');
	    	
    	}else{
    		$(this).append('<section id="chat-SSO"></section>');/*57671 - Chat SSO*/
	    		
    	}
    	$('#chat-SSO').html('<div class="loadingAjax"><p>Carregando conte&uacute;do...</p></div>');
    	var urlChat = '/portal/site/MinhaOi/ChatSSO';
				$.ajax({
					url: urlChat,
					dataType: 'html',
					type: 'get',
					async: true,
					success: function(dados,textStatus, request) {
						$('#chat-SSO').empty().append(innerShiv(dados,false));
						chatSSO();	
					}
				});
    });
    
     if(!String.prototype.trim){  /*57671 - Chat SSO - mÃ©todo com suporte para IEs*/
    	String.prototype.trim = function(){  
         return this.replace(/^\s*([\S\s]*?)\s*$/, '$1');  
         };  
    } 
    /*END 57671 - Chat SSO*/
	
	/*ARS - Status Pagamento - 12/11/13 */
	
	$('.account-faturaDetalhesConta td.statusPagamento b').each(function() {

		if($(this).text() == 'Em Aberto'){
			$(this).css('color','red');
		}

	});
	
	/*ARS - Status Pagamento */
	
	$(window).load(function() {
		var titPag = $('h2.tit-contaTotal:contains(Total)');
		if(titPag){
			   
			$('.account-faturaDetalhesConta td.statusPagamento b:contains(Pago)').each(function(){	  
				$(this).parent().find('span').removeClass('tooltipIco');
			});

			/* 82115 - 27/05/2014 */
			$('.account-faturaDetalhesConta td.statusPagamento b:contains(Paga)').each(function(){	  
				$(this).parent().find('span').removeClass('tooltipIco');	  
			});
			/* 82115 - 27/05/2014 */

		}

	});
	/*ARS - Status Pagamento */

    $('.consulta-periodo .erro').html('');//53544 e 55688 - PRD - 30/11/2012
	
	//ARS Recarga Automatica - 08/11/13
	$(".recarga-automatica #textTerminal").bind("cut copy paste",function(event) {
		event.preventDefault();
	});
	
	$(".recarga-automatica #textTerminal").keypress(function(event){
	    var mregex = /^[0-9]{9}$/;
		var mterminal = $(".recarga-automatica #textTerminal").val();
		if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
			event.preventDefault();
		}
	    if(event.which != 8 && mregex.test(mterminal)){
			event.preventDefault();
		}
	});
	
	$("#subscribeRecarga").submit(function(){
		var mterminal = $(".recarga-automatica #textTerminal").val();

		var qtdOpts = $('input[name=selectedTerminal]', '#subscribeRecarga').each(function(){}).length;
		$('input[name=selectedTerminal]', '#subscribeRecarga').each(function(){})[qtdOpts-1].value = $("#defaultDDD").val() + mterminal;

		var valSelected = $('input[name=selectedTerminal]:checked', '#subscribeRecarga').val();

		if($(".recarga-automatica input.iptRadio").is(":checked") && valSelected.length > 12 && valSelected.length < 16){
			return true;
		}else{
			$("#errorMessage").show();
			return false;
		}
	});
	//FIM ARS Recarga Automatica - 08/11/13

	/* BEGIN 55460 */
    //if(navigator.userAgent.toLowerCase().indexOf('chrome') > -1 || navigator.userAgent.toLowerCase().indexOf('msie') == -1){
    if(!(navigator.userAgent.toLowerCase().indexOf('firefox') != -1)){
        $('#saldoDados.account-saldoAtual tfoot.pacoteComplementar').addClass('chrome');
        $('#saldoDados.account-saldoAtual tfoot.pacoteComplementar.chrome').after('<div class="boxEspacamento"></div>');
    }else{
        $('#saldoDados.account-saldoAtual tfoot.pacoteComplementar').removeClass('chrome');
    }
    /* END 55460 */

});

function addInputBorders() {
    $('input[type="text"],input[type="password"],input[type="date"],input[type="tel"],input[type="email"]').not("#conta-online :input, #alteracaoDadosAcesso :input").addClass('ipt');
    $('form#solicitarFaleConosco :input').each(function(){ $(this).removeClass('ipt')}); //Change for PRJ4740
    $('input.selectbox').removeClass('ipt')
    $('.ipt').wrap('<span class="wrapinput" />');
    $('input[type="radio"]').addClass('iptRadio');
    
    $('.wrapinput').prepend('<span class="leftinput" />');
    $('.wrapinput').append('<span class="rightinput" />');
    
    $('input[type="checkbox"]').addClass("inputCheckBox");
    $('label.inlineCheckbox').removeClass("LabelCheckBox");
}

function collapseExpand() {
    
     // expandCollapse: vem encolhida e pode ser expandida
     $('table.table-expandCollapse').each(function(i, table) {
     var tbody = $('tbody', table).hide();
     
     $('thead a', table).click(function(event) {
     event.preventDefault();
     tbody.toggle();
     reloadSelectboxPosition()
     })
     reloadSelectboxPosition()
     });
     
     // collapseExpand: vem expandida e pode ser encolhida
     $('table.table-collapseExpand').each(function(i, table) {
     var tbody = $('tbody', table).show();
     
     $('thead a', table).click(function(event) {
			event.preventDefault();
			tbody.toggle();
			reloadSelectboxPosition()
     })
			reloadSelectboxPosition()
     }); 
    
    $('th a').click(function(event) {
     event.preventDefault();
     var that = $(this);
     
     if(that.hasClass('th-open')) {
     that.text("+ Detalhes").attr('title','Detalhes').removeClass('th-open').addClass('th-close');
     } else {
     that.text("- Ocultar").attr('title','Ocultar').removeClass('th-close').addClass('th-open');
     }
     reloadSelectboxPosition();
     ieFixes();
     });

}

function trataBotao() {
    $('.button').append('<span />');
    
}
    
// Agrupa scripts pra correcao de falta de suporte a css e outros problemas em versoes antigas de IE's.
function ieFixes(){

    $('.account-cartao-atendimentoOnline ul li:first-child, .account-cartao-atendimentoOnline ul li:eq(1)').addClass('noBorder')
    $(".message-box p:first-child").addClass("first-child");
    $('.accountBox .account-overview tr:first-child, .data-tabela tr:first-child, .data-tabela h6:first-child').addClass('first-child');
    $('.accountBox .account-overview tr:last-child, .data-tabela tr:last-child').addClass('last-child');
    $('.accountBox table.oiAgenda tr:last-child').addClass('last-child');
    $('.accountBox .account-moreActions ul:nth-child(odd)').addClass('nth-child-odd');
    $('.account-tudoParaFixo ul li:nth-child(3n+1)').addClass('nth-child3n');
    $('.account-cartao-atendimentoOnline ul li:nth-child(odd)').addClass('nth-child-odd');
    $('.account-cartao-recargaOnline-info .roundedBox div:first-child').addClass('first-child');
    $('.account-cartao-recargaOnline-info .roundedBox div:last-child').addClass('last-child');
    $('#userActions li:last-child').addClass('last-child');
    $('ul.rounded1rowRecarga li:last-child').addClass('last-childRecarga');
    $('ul.roundedStartRecarga li:last-child').addClass('last-childStartRecarga');
    $('#login .pageLogin section.loginArea .linksLogin li:last').addClass('lastLinksLogin');


}

// Simula o uso de 'placeholder' em browsers sem suporte
function enablePlaceholder(){
    if (!elementSupportsAttribute('input', 'placeholder')) {
        $("textarea, input").each(function(){
            if ($(this).val().replace(/\s/gi, '') == "" && $(this).attr("placeholder")) 
                $(this).val($(this).attr("placeholder")).addClass("placeholder")
        }).focus(function(){
            if ($(this).val() == $(this).attr("placeholder")) 
                $(this).val("").removeClass("placeholder")
        }).blur(function(){
            if ($(this).val().replace(/\s/gi, '') == "" && $(this).attr("placeholder")) 
                $(this).val($(this).attr("placeholder")).addClass("placeholder")
        });
    }
    // Limpa os campos com placeholder simulado no envio dos formularios
    $('form').submit(function(){
        $(this).find('textarea,input').each(function(){
            if ($(this).val() == $(this).attr('placeholder')) 
                $(this).val('');
        });
    });
}

// Testa o suporte de um atributo em um elemento
function elementSupportsAttribute(element, attribute){
    var test = document.createElement(element);
    if (attribute in test) {
        return true;
    }
    else {
        return false;
    }
};

// Solucoes para adequacao de funcoes em formularios. 
function sweetForm(){

    $('form.sweetForm input[type="text"], form.sweetForm input[type="password"]').each(function(){
        if ($(this).attr("tooltip")) {
            $(this).after('<span class="tooltip"/><div class="tooltip">' + $(this).attr("tooltip") + '</div>')
        }
    })

    $("form.sweetForm span.tooltip").hover(function(){
        $(this).next("div.tooltip").css({
            left: $(this).position().left + 35
        }).stop(true, true).animate({
            opacity: "show",
            left: $(this).position().left + 25
        }, "slow");
    }, function(){
        $(this).next("div.tooltip").stop(true, true).animate({
            opacity: "hide",
            left: $(this).position().left + 45
        }, "fast");
    });
    $("form.sweetForm span.tooltip").parent().css('position','relative');

    $('form.sweetForm input[type="text"], form.sweetForm input[type="password"], form.sweetForm input[type="tel"], form.sweetForm input[type="date"], form.sweetForm input[type="email"]').addClass('typeText');
}

// Controles de edicao de contatos de agenda
function formInlineEdit(){
    $('.oiAgenda-editar').on('click', function(){
        var editing = $('tr.agenda-editForm');
        if (editing.length) {
            var tdtel = editing.find('td:eq(0)');
            var tdnome = editing.find('td:eq(1)');
            var tdedit = editing.find('td:eq(2)');
            tdtel.html(tdtel.find('input').val());
            tdnome.html(tdnome.find('input').val());
            tdedit.html('<a href="#" title="Editar" class="oiAgenda-editar">Editar</a> | <a href="#agenda-excluirContato" title="Excluir" class="oiAgenda-excluir">Excluir</a>');
            editing.removeClass('agenda-editForm');
        }
        
        $(this).parents('tr').addClass('agenda-editForm');
        var irmaos = $(this).parents('td').siblings('td');
        var vtel = $(irmaos[0]).html();
        var vnome = $(irmaos[1]).html();
        $(irmaos[0]).html('<input type="text" name="telefone" value="' + vtel + '"/>').children().addClass('ipt').wrap('<span class="wrapinput" />').parents('span.wrapinput').prepend('<span class="leftinput" />').append('<span class="rightinput" />');
        $(irmaos[1]).html('<input type="text" name="telefone" value="' + vnome + '"/>').children().addClass('ipt').wrap('<span class="wrapinput" />').parents('span.wrapinput').prepend('<span class="leftinput" />').append('<span class="rightinput" />');
		
        $(this).parents('td').html('<a href="#agenda-incluirContato" title="Salvar" class="oiAgenda-salvar">Salvar</a>');

        atribuiFancyboxes();
        return false;
    });
}

function userActions(){
    $('#userActions li').hover(function(){
        $(this).find('div').show()
    }, function(){
        $(this).find('div').hide()
    });
}

function manageFaturaDetalhada() {

    // Controle de Expand-collapse das faturas
	$('.detalhamentoConsumo-tipoServico .detalhamentoConsumo').hide();
	$('.detalhamentoConsumo-tipoServico h4').append('<a title="Detalhes">+ Detalhes</a>');
		
	$( ".detalhamentoConsumo-tipoServico h4 a" ).on( "click", function() {
		
	  if($(this).text() == "- Ocultar") {
		$(this).removeClass('OcultarBt');
		$(this).text("+ Detalhes").attr('title','Detalhes').parents('.detalhamentoConsumo-tipoServico').find('.detalhamentoConsumo').hide();
		reloadSelectboxPosition();
	  } else {
		$(this).text("- Ocultar").attr('title','Ocultar').parents('.detalhamentoConsumo-tipoServico').find('.detalhamentoConsumo').show();
		$(this).addClass('OcultarBt');
		reloadSelectboxPosition();
	  }
	  
	});

	$('.detalhamentoConsumo-tipoServico').eq(0).find('a').click();

    // Filtros de ligacao
    $('.detalhamentoConsumo-filtros-avancado').hide();
	
	$( ".detalhamentoConsumo-filtros-Ligacao a" ).on( "click", function() {
		
	  if($(this).text() == "Ocultar filtros") {
        $(this).parents('.detalhamentoConsumo-filtros').find('.detalhamentoConsumo-filtros-avancado').hide();
        $(this).text("Ver filtros");
        reloadSelectboxPosition();
        return false;
	  } else {
        $(this).parents('.detalhamentoConsumo-filtros').find('.detalhamentoConsumo-filtros-avancado').show();
        $(this).text("Ocultar filtros");
        reloadSelectboxPosition();
        return false;
	  }
	  
	});

    if($.fancybox) $("a.bt-fatura-cadastrarNumero").fancybox({
        'scrolling': 'no',
		'autoDimensions': true,
        'padding': 15,
		'centerOnScroll': true,
        'titleShow': false
    });
    $('#fancybox-content .closeFancyBox').on('click', function(){
        if($.fancybox) $.fancybox.close();
        return false;
    })

}

function setasVencimentoAnaliseConta(){
    $('.previousVencimento, .nextVencimento').bind('click', function(){
        $('.previousVencimento, .nextVencimento').show();
        var abaAtiva = $('ul.slideVencimento li.ativo');
        if (abaAtiva.length) {
            var proximaAba = abaAtiva.next();
            var abaAnterior = abaAtiva.prev();
        }
        else {
            var proximaAba = $('ul.slideVencimento li:first');
            var abaAnterior = proximaAba;
        }
        var novaAbaAtiva = $(this).hasClass('previousVencimento') ? abaAnterior : proximaAba;
        if (novaAbaAtiva.length) {
            abaAtiva.removeClass('ativo');
            novaAbaAtiva.addClass('ativo');
            if (!novaAbaAtiva.next().length)
                $('.nextVencimento').hide();
            if (!novaAbaAtiva.prev().length)
                $('.previousVencimento').hide();
        }

        return false;
    });
}

// Corrige o problema nas caixas de selecao customizadas que nao sao exibidas quando a pagina e carregada.
function reloadSelectboxPosition(){
    $('input.selectbox').each(function(){
        var pos = $(this).position()
        $(this).nextAll('.selectbox-wrapper').css({
            left: pos.left,
            top: pos.top + 25
        })
    });
}

//leo.carvalho
function linksAjax(){
	$('a.ajaxLink').on('submit', handle); $('a.ajaxLink').on('click', handle); function handle(){
		var lnk = $(this);
		var linkhref = lnk.attr('href').split('#');
		var lurl = linkhref[0];
		var destsubdiv = $('#' + linkhref[1]);
		if(destsubdiv.length){
			destsubdiv.show();
			$.get(linkhref[0], function(dados){
				$(destsubdiv).empty().append(innerShiv(dados,false));
				ieFixes();
				trataConteudo(destsubdiv);
				tabsPanelsResize();
				validaFrameTHS();
				createTooltipIcos();
				collapseExpand();
				trataBotao();
				atribuiTabs();
				paymentVazio();
				boxDaccCtOnline();
				reloadSelectboxPosition();
				addInputBorders();
			});
			return false;
		}
	};
}

function validaFrameTHS(){
    if($("#frameFormTHS").length > 0){
        callFormTHS($("#frameFormTHS").attr('src'),"100%","900");
    }
}

function createTooltipIcos(){
	//Solucao temporaria para IEs - bug tooltip saldo
	//if ($.browser.msie) { 04/08/2014
	if((window.navigator.userAgent.indexOf() == -1)){
        // Tooltip left
	    $('.tooltip').each(function(){
				if ($(this).attr("tooltip")) {
					if(!$(this).next('span.tooltipIco').length) $(this).after('<span class="tooltipIco"/><div class="tooltip">' + $(this).attr("tooltip") + '</div>')
	      };
	    });
		$("span.tooltipIco").hover(function () {
				position = $(this).position();
				$(this).next("div.tooltip").css({
					top: position.top - 15,
					left: position.left + 35,
					opacity: 0,
					display: 'block'
				}).animate({
					opacity: 1,
					left: position.left + 25
				}, "slow", function(){
					$(this).show();
				});
			}, function(){
				$(this).next("div.tooltip").stop(true, true).animate({
					opacity: 0,
					left: $(this).position().left + 45
				}, "fast", function(){
					$(this).hide();
				});
			});
			// Tooltip big teste IE
			    $('.Destaque.tooltipBig , span.tooltipBig').each(function(){
			        if ($(this).attr("tooltip")) {
			            $(this).append('<span class="tooltipIcoB icoBig"/><div class="tooltipBig">' + $(this).attr("tooltip") + '</div>')
			        }
			    }) 
				// Tooltip bottom sequencial - leo.carvalho
			    $('.tooltipBottom').each(function(i){
			        if ($(this).attr("tooltip")) {
						var icon = $('<span class="tooltipIcoB icoBottom"/>').data('tid', i);
			            if(!$(this).next('span.tooltipIcoB').length) $(this).append(icon);
						$('body').append('<div class="tooltipBottom" id="tooltip-'+i+'">' + $(this).attr("tooltip") + '</div>');
			        }
			    })
				 //Tooltip ? para demostrativo de contas e outros, exibe conteÃºdo prÃ³ximo
				  $("span.tooltipIcoB.icoBig").hover(function(){
				        position = $(this).position();
				        $(this).next("div.tooltipBig").css({
				            top: position.top + 20,
				            left: position.left - 150,
				            opacity: 0,
				            display: 'block'
				        }).stop(true, true).animate({
				            opacity: 1,
				            left: position.left - 150
				        }, "slow", function(){
				            $(this).show();
				        });
				    }, function(){
				        $(this).next("div.tooltipBig").stop(true, true).animate({
				            opacity: 0,
				            top: $(this).position().top + 20
				        }, "fast", function(){
				            $(this).hide();
				        });
				    });
					//Tooltip ? para demostrativo de contas e outros, sequencial variando conforme nÃºmeros de tooltips
				    $("span.tooltipIcoB.icoBottom").hover(function(){
				        position = $(this).offset();

				        $('#tooltip-' + $(this).data('tid')).css({
				            top: position.top + 20,
				            left: position.left - 150,
				            opacity: 0,
				            display: 'block'
				        }).stop(true, true).animate({
				            opacity: 1,
				            left: position.left - 150
				        }, "slow", function(){
				            $(this).show();
				        });
				    }, function() {
				        $('#tooltip-' + $(this).data('tid')).stop(true, true).animate({
				            opacity: 0,
				            top: $(this).offset().top + 20
				        }, "fast", function(){
				            $(this).hide();
				        });
				    });
				
    }else{
    	// Tooltip left
	    $('.tooltip').each(function(){
				if ($(this).attr("tooltip")) {
					if(!$(this).next('span.tooltipIco').length) $(this).after('<span class="tooltipIco"/><div class="tooltip">' + $(this).attr("tooltip") + '</div>')
	      };
	    });
		
	   // Tooltip big
	    $('.tooltipBig').each(function(){
	        if ($(this).attr("tooltip")) {
	            if(!$(this).next('span.tooltipIcoB').length) $(this).after('<span class="tooltipIcoB icoBig"/><div class="tooltipBig">' + $(this).attr("tooltip") + '</div>')
	        }
	    })
	    // Tooltip bottom sequencial - leo.carvalho
	    $('.tooltipBottom').each(function(i){
	        if ($(this).attr("tooltip")) {
				var icon = $('<span class="tooltipIcoB icoBottom"/>').data('tid', i);
	            if(!$(this).next('span.tooltipIcoB').length) $(this).after(icon);
				$('body').append('<div class="tooltipBottom" id="tooltip-'+i+'">' + $(this).attr("tooltip") + '</div>');
	        }
	    })

	    $("span.tooltipIco").hover(function () {
				position = $(this).position();
				$(this).next("div.tooltip").css({
					top: position.top - 15,
					left: position.left + 35,
					opacity: 0,
					display: 'block'
				}).animate({
					opacity: 1,
					left: position.left + 25
				}, "slow", function(){
					$(this).show();
				});
			}, function(){
				$(this).next("div.tooltip").stop(true, true).animate({
					opacity: 0,
					left: $(this).position().left + 45
				}, "fast", function(){
					$(this).hide();
				});
			});
	  //Tooltip ? para demostrativo de contas e outros, exibe conteÃºdo prÃ³ximo 
	  $("span.tooltipIcoB.icoBig").hover(function(){
	        position = $(this).position();
	        $(this).next("div.tooltipBig").css({
	            top: position.top + 20,
	            left: position.left - 150,
	            opacity: 0,
	            display: 'block'
	        }).stop(true, true).animate({
	            opacity: 1,
	            left: position.left - 150
	        }, "slow", function(){
	            $(this).show();
	        });
	    }, function(){
	        $(this).next("div.tooltipBig").stop(true, true).animate({
	            opacity: 0,
	            top: $(this).position().top + 20
	        }, "fast", function(){
	            $(this).hide();
	        });
	    });
	    //Tooltip ? para demostrativo de contas e outros, sequencial variando conforme nÃºmeros de tooltips
	    $("span.tooltipIcoB.icoBottom").hover(function(){
	        position = $(this).offset();

	        $('#tooltip-' + $(this).data('tid')).css({
	            top: position.top + 20,
	            left: position.left - 150,
	            opacity: 0,
	            display: 'block'
	        }).stop(true, true).animate({
	            opacity: 1,
	            left: position.left - 150
	        }, "slow", function(){
	            $(this).show();
	        });
	    }, function() {
	        $('#tooltip-' + $(this).data('tid')).stop(true, true).animate({
	            opacity: 0,
	            top: $(this).offset().top + 20
	        }, "fast", function(){
	            $(this).hide();
	        });
	    });

	}
	
	//Novo tooltip dinamico para conteudos do topo e miolo, Tooltip ? - jun 2012
	$(".tooltipIco.dinam").next().hide();
	$(".tooltipIco.dinam").next().next().hide();
	$(".tooltipIco.dinam").hover(function(){
		text1 = $(this).next('.text-tooltipX').children().html().substring(0,92);	//ars ultimas contas - 23/06/2014
		text_compl = $(this).next('.text-tooltipX').children().html().substring(92);//ars ultimas contas - 23/06/2014
		$(this).next().next().find(".tip-top .tip-title1").html(text1);
		$(this).next().next().find(".tip .tip-title").html(text_compl);
	
	        position = $(this).position();
	        $(this).next().next("div.tooltipX").css({
	            top: position.top - 45,
	            left: position.left + 30,
	            opacity: 0,
	            display: 'block'
	        }).stop(true, true).animate({
	            opacity: 1,	
	            left: position.left + 20
	        }, "slow", function(){
	            $(this).show();
	        });
	    }, function(){
	        $(this).next().next("div.tooltipX").stop(true, true).animate({
	            opacity: 0,
	            top: $(this).position().top - 45
	        }, "fast", function(){
	            $(this).hide();
	        });
	 });
	//Novo tooltip dinamico  warning Tooltip !
	$(".tooltipIco.warning.dinam").hover(function(){
		text1 = $(this).next('.text-tooltipX').children().html().substring(0,96);
		text_compl = $(this).next('.text-tooltipX').children().html().substring(96);
		$(this).next().next().find(".tip-top .tip-title1").html(text1);
		$(this).next().next().find(".tip .tip-title").html(text_compl);

	        position = $(this).position();
	        $(this).next().next("div.tooltipX").css({
	            top: position.top - 45,
	            left: position.left + 80,
	            opacity: 0,
	            display: 'block'
	        }).stop(true, true).animate({
	            opacity: 1,	
	            left: position.left + 30
	        }, "slow", function(){
	            $(this).show();
	        });
	    }, function(){
	        $(this).next().next("div.tooltipX").stop(true, true).animate({
	            opacity: 0,
	            top: $(this).position().top - 45
	        }, "fast", function(){
	            $(this).hide();
	        });
	 });
	//fim tooltip dinamico
	//554542 -  Tooltip dinamico  3ps com conteudos em listas ! - leo.carvalho - 02/03/2013
	$(".listDinam").next().addClass('Vert');
	$(".tooltipIco.listDinam").hover(function(){
	        position = $(this).position();
	        $(this).next("div.tooltipX").css({
	            left: position.left + 2,
	            opacity: 0,
	            display: 'block'
	        }).stop(true, true).animate({
	            opacity: 1,	
	            left: position.left -91
	        }, "slow", function(){
	            $(this).show();
	        });
	    }, function(){
	        $(this).next("div.tooltipX").stop(true, true).animate({
	            opacity: 0
	        }, "fast", function(){
	            $(this).hide();
	        });
	 });
	//fim tooltip dinamico
	
	//80681 - tooltip home oi tv
	$(".tooltipTv").hide();
	$("span.tooltipIco.topDinam").hover(function(){

		position = $(this).position();
		$(".tooltipTv").css({
            //top: position.top + 82,
            //left: position.left + 2
            position: 'absolute',
            top: position.top -110,
            left: position.left - 20
        }).stop(true, true).show();

    }, function() {
		$(".tooltipTv").stop(true, true).hide();
    });
    //fim tooltip home oi tv
	
}

function atribuiFancyboxes(){
  if($.fancybox){
    $("table.oiAgenda .oiAgenda-excluir").fancybox({
			'data': $('#agenda-excluirContato').html(),
			'scrolling': 'no',
			'autoDimensions': true,
			'centerOnScroll': true,
			'titleShow': false
		});
    $("table.oiAgenda .oiAgenda-salvar").fancybox({
			'data': $('#agenda-incluirContato').html(),
			'scrolling': 'no',
			'autoDimensions': true,
			'centerOnScroll': true,
			'titleShow': false
		});
    $("p.oiAgenda a.button").fancybox({
			'scrolling': 'no',
			'padding': 15,
			'autoDimensions': true,
			'centerOnScroll': true,
			'titleShow': false
		});
    $('#fancybox-content .closeFancyBox').on('click', function(){
			if($.fancybox) $.fancybox.close();
			return false;
		})
    $(".sweetForm .modals").fancybox({
			'scrolling': 'no',
			'autoDimensions': true,
			'padding': 30,
			'titleShow': false,
			'centerOnScroll': true,
			'showCloseButton': false
		});
    $(".sweetForm .submitModal").each(function(){
			if($.fancybox) $(this).fancybox({
				'scrolling': 'no',
				'autoDimensions': true,
				'padding': 30,
				'titleShow': false,
				'showCloseButton': false,
				'type': 'inline',
				'centerOnScroll': true,
				'content': $(this).attr('title')
			});
    });
	
	$(window).load(function() {
		$(".recarga-automatica a#ativaRecarga").fancybox({
			'scrolling': 'no',
			'padding': 15,
			'autoDimensions': true,
			'centerOnScroll': false,
			'showCloseButton': false,
			'titleShow': false,
			'onComplete': function() {
				$("#fancybox-wrap").css({
				'top': 20 + $(window).scrollTop() +'px'
				});
			}			
		});
	});

  }
}

function atribuiTabs(){
	
	var ctTabs = $('article.ctTab').hide();
	$('ul.navNumeros + .ctTab').show();
	$('ul.navNumeros li a').click(function(){
			ctTabs.hide();
			$('#' + this.href.split('#')[1]).show();
			$('ul.navNumeros li').removeClass('ativo')
			$(this).parent().addClass('ativo');
			reloadSelectboxPosition();
			return false;
	});
	ctTabs.eq(0).show();
	$('div.aba').hide();
	$('ul.overview-tabs + .aba, .abas + .aba').show();

	$('ul.abas li a').click(function(){
			$('div.aba').hide();
			$('#' + this.href.split('#')[1]).show();
			$('ul.abas li').removeClass('tabActived')
			$(this).parent().addClass('tabActived');
			reloadSelectboxPosition();
			return false;
	});

	$('ul.overview-tabs li a').click(function(){
			$('div.aba').hide();
			$('#' + this.href.split('#')[1]).show();
			$('ul.overview-tabs li').removeClass('active');
			$(this).parent().addClass('active');
			reloadSelectboxPosition();
			return false;
	});
	//Abas miolo - Codigo de Barras - 55772
			$('.tabs-miolo li a').click(function(){
				$('.tabs-miolo li').removeClass('active')
				$(this).parent().addClass('active');
				reloadSelectboxPosition();
			});
	
}

function fecharfancy() {
    if($.fancybox) $.fancybox.close()
}

function paymentVazio(){
		$('ul.details-payment li').each(function(){
			if($(this).html() == '&nbsp;' || $(this).html() == null){
			    $(this).hide();
			}
	});	
}
/* INICIO PLUGIN MASKED-INPUT (versao 1.3.1) */

(function($) {
	var pasteEventName = (window.navigator.userAgent.indexOf() == -1 ? 'paste' : 'input') + ".mask";
	var iPhone = (window.orientation != undefined);

	$.mask = {
		//Predefined character definitions
		definitions: {
			'9': "[0-9]",
			'a': "[A-Za-z]",
			'*': "[A-Za-z0-9]"
		},
		dataName:"rawMaskFn"
	};

	$.fn.extend({
		//Helper Function for Caret positioning
		caret: function(begin, end) {
			if (this.length == 0) return;
			if (typeof begin == 'number') {
				end = (typeof end == 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						var range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		unmask: function() { return this.trigger("unmask"); },
		mask: function(mask, settings) {
			if (!mask && this.length > 0) {
				var input = $(this[0]);
				return input.data($.mask.dataName)();
			}
			settings = $.extend({
				placeholder: "_",
				completed: null
			}, settings);

			var defs = $.mask.definitions;
			var tests = [];
			var partialPosition = mask.length;
			var firstNonMaskPos = null;
			var len = mask.length;

			$.each(mask.split(""), function(i, c) {
				if (c == '?') {
					len--;
					partialPosition = i;
				} else if (defs[c]) {
					tests.push(new RegExp(defs[c]));
					if(firstNonMaskPos==null)
						firstNonMaskPos =  tests.length - 1;
				} else {
					tests.push(null);
				}
			});

			return this.trigger("unmask").each(function() {
				var input = $(this);
				var buffer = $.map(mask.split(""), function(c, i) { if (c != '?') return defs[c] ? settings.placeholder : c });
				var focusText = input.val();

				function seekNext(pos) {
					while (++pos <= len && !tests[pos]);
					return pos;
				};
				function seekPrev(pos) {
					while (--pos >= 0 && !tests[pos]);
					return pos;
				};

				function shiftL(begin,end) {
					if(begin<0)
					   return;
					for (var i = begin,j = seekNext(end); i < len; i++) {
						if (tests[i]) {
							if (j < len && tests[i].test(buffer[j])) {
								buffer[i] = buffer[j];
								buffer[j] = settings.placeholder;
							} else
								break;
							j = seekNext(j);
						}
					}
					writeBuffer();
					input.caret(Math.max(firstNonMaskPos, begin));
				};

				function shiftR(pos) {
					for (var i = pos, c = settings.placeholder; i < len; i++) {
						if (tests[i]) {
							var j = seekNext(i);
							var t = buffer[i];
							buffer[i] = c;
							if (j < len && tests[j].test(t))
								c = t;
							else
								break;
						}
					}
				};

				function keydownEvent(e) {
					var k=e.which;

					//backspace, delete, and escape get special treatment
					if(k == 8 || k == 46 || (iPhone && k == 127)){
						var pos = input.caret(),
							begin = pos.begin,
							end = pos.end;
						
						if(end-begin==0){
							begin=k!=46?seekPrev(begin):(end=seekNext(begin-1));
							end=k==46?seekNext(end):end;
						}
						clearBuffer(begin, end);
						shiftL(begin,end-1);

						return false;
					} else if (k == 27) {//escape
						input.val(focusText);
						input.caret(0, checkVal());
						return false;
					}
				};

				function keypressEvent(e) {
					var k = e.which,
						pos = input.caret();
					if (e.ctrlKey || e.altKey || e.metaKey || k<32) {//Ignore
						return true;
					} else if (k) {
						if(pos.end-pos.begin!=0){
							clearBuffer(pos.begin, pos.end);
							shiftL(pos.begin, pos.end-1);
						}

						var p = seekNext(pos.begin - 1);
						if (p < len) {
							var c = String.fromCharCode(k);
							if (tests[p].test(c)) {
								shiftR(p);
								buffer[p] = c;
								writeBuffer();
								var next = seekNext(p);
								input.caret(next);
								if (settings.completed && next >= len)
									settings.completed.call(input);
							}
						}
						return false;
					}
				};

				function clearBuffer(start, end) {
					for (var i = start; i < end && i < len; i++) {
						if (tests[i])
							buffer[i] = settings.placeholder;
					}
				};

				function writeBuffer() { return input.val(buffer.join('')).val(); };

				function checkVal(allow) {
					//try to place characters where they belong
					var test = input.val();
					var lastMatch = -1;
					for (var i = 0, pos = 0; i < len; i++) {
						if (tests[i]) {
							buffer[i] = settings.placeholder;
							while (pos++ < test.length) {
								var c = test.charAt(pos - 1);
								if (tests[i].test(c)) {
									buffer[i] = c;
									lastMatch = i;
									break;
								}
							}
							if (pos > test.length)
								break;
						} else if (buffer[i] == test.charAt(pos) && i!=partialPosition) {
							pos++;
							lastMatch = i;
						}
					}
					if (!allow && lastMatch + 1 < partialPosition) {
						input.val("");
						clearBuffer(0, len);
					} else if (allow || lastMatch + 1 >= partialPosition) {
						writeBuffer();
						if (!allow) input.val(input.val().substring(0, lastMatch + 1));
					}
					return (partialPosition ? i : firstNonMaskPos);
				};

				input.data($.mask.dataName,function(){
					return $.map(buffer, function(c, i) {
						return tests[i]&&c!=settings.placeholder ? c : null;
					}).join('');
				})

				if (!input.attr("readonly"))
					input
					.one("unmask", function() {
						input
							.unbind(".mask")
							.removeData($.mask.dataName);
					})
					.bind("focus.mask", function() {
						focusText = input.val();
						var pos = checkVal();
						writeBuffer();
						var moveCaret=function(){
							if (pos == mask.length)
								input.caret(0, pos);
							else
								input.caret(pos);
						};
						(window.navigator.userAgent.indexOf() == -1 ? moveCaret:function(){setTimeout(moveCaret,0)})();
					})
					.bind("blur.mask", function() {
						checkVal();
						if (input.val() != focusText)
							input.change();
					})
					.bind("keydown.mask", keydownEvent)
					.bind("keypress.mask", keypressEvent)
					.bind(pasteEventName, function() {
						setTimeout(function() { input.caret(checkVal(true)); }, 0);
					});

				checkVal(); //Perform initial check for existing values
			});
		}
	});
})(jQuery);

/* FIM PLUGIN MASKED-INPUT (versao 1.3.1) */

//Calculate date difference in days 53544 e 55688.
function validaPeriodo(){

	DAY = 1000 * 60 * 60  * 24

	dataStart = $("input[type=text][name=data-inicio]").val();
	dataEnd = $("input[type=text][name=data-fim]").val();

	//Select dates in the correct format for the Diff - leo.carvalho
	var nStart = dataStart.toString().split('/');
	Nova1 = nStart[1]+"/"+nStart[0]+"/"+nStart[2];

	var nEnd = dataEnd.toString().split('/');
	Nova2 = nEnd[1]+"/"+nEnd[0]+"/"+nEnd[2];

	d1 = new Date(Nova1)
	d2 = new Date(Nova2)
	//Difference between two dates in days - leo.carvalho
	days_passed = Math.round((d2.getTime() - d1.getTime()) / DAY)	
	
	$('.consulta-periodo .erro').hide();
	//Checks date range before submitting 
	$("form.calendar").submit(function() {
	
		if((dataStart == "") || (dataEnd == "")){/*ALTERADO por solicitaÃ§Ã£o usuÃ¡ria para PRD - 30/11/2012*/
			$('.consulta-periodo .erro').html('<strong>Informe o perÃ­odo de consulta.</strong>');
			$('.consulta-periodo .erro').show();
      			return false;
			
		}else{
			//Alterado por solicitaÃ§Ã£o da Ã¡rea usuaria
			if((d1 > d2 )){
					//Se a data inicial for maior que a data final - 30/11/2012 
	      			$('.consulta-periodo .erro').html('<strong>DataÂ inicial maior que a data final.</strong>');
	      			$('.consulta-periodo .erro').show();
	      				return false;	
			}else{
				if(days_passed >= 0 && days_passed <= 90){//condiÃ§Ã£o 90 dias, perÃ­odo mÃ¡ximo -
	        			return true;
		      	}else{
					//Displays message if period exceeds 90 days -
	      			$('.consulta-periodo .erro').html('<strong>PerÃ­odo de consulta superior a 3 meses.</strong> Favor consultar novamente.');
	      			
	      			$('.consulta-periodo .erro').show();
	      				return false;
				}	
			}	
		}
		
    });
	$(".acp-instalcao form.calendar").submit(function() {
		//condition 30 days
		if(days_passed > 0 && days_passed <= 30){
        	return true;
      	}else{
			//Displays message if period exceeds 90 days
      		$('.consulta-periodo .erro').show();
      		return false;
		}
    });
}

//SoluÃ§Ã£o para chamada de js dentro de requisiÃ§Ã£o Ajax, 57671 - ChatSSO
function PopUpAjax(){
	//Chama script de popUp - atende IE7 e IE8 tambÃ©m
	$.getScript("/ArquivosEstaticos/MinhaOi/scripts/jquery.popupWindow.js", function() {
		
		$('#sidebar-chat a').popupWindow({ 
		height:400, 
		width:470, 
		top:50, 
		left:50 
		});
			
  });		
}
//SoluÃ§Ã£o para requisiÃ§Ã£o Ajax, 57671 - ChatSSO - trata conteÃºdo do artigo do VCM
function chatSSO(){

	$('#chat-SSO').each(function(){
		if($('#chat-SSO div div').is(':visible')){
    	contChat = $('#chat-SSO div div').html().trim();
					if(contChat == ""){
						$('#geral').removeClass('cadastro');
						
					}else{PopUpAjax();
						return true;
						}
		}else{
	   		 $('#geral').removeClass('cadastro');
			 $('.area-sso #geral aside .meusDados').css({'margin':'0', 'padding-left': '20px', 'width': '210px'});	/*Esteira Ãgil - c.s. - 08/10/13*/						    
	   	}    
    });
}
//55772
/*function tabsMiolo(){
	setTimeout(function(){
        $('.tabs-miolo').each(function(){
			
	abaOne = $(this).find('.aba.first a');
	abaTwo = $(this).find('.kin a');
	
	abaOne.attr('href', window.location.href.split('D')[0] + 'D&faturaFixo=true&faturaFixo=false');
	abaTwo.attr('href', window.location.href.split('D')[0] + 'D&faturaFixo=true&faturaFixo=true');
	
		});
	}, 3000);
}*/
$(function(){
	//Box de aviso collapse Dacc, Ct Online, Codigo de Barras e outros 55772
	var avisoServico = $('.warning-box');
	
	var listaServicos = avisoServico.find(".srv-fixo"); 
	var linkExtras = avisoServico.find('.servicos-fixo');
	var abreLink = $("#open-servicos");
	var fechaLink = $("#close-servicos");
	fechaLink.hide();
	listaServicos.hide();
	
	linkExtras.click(function(event){
			linkExtras.toggleClass("open");
			listaServicos.slideToggle();
			if(linkExtras.hasClass("open")){
				abreLink.hide();
				fechaLink .show();
			}else{
				fechaLink .hide();
				abreLink.show();
				}
        return false;
		});
	//END 55772 Box Warning

});

//funÃ§Ã£o para montar os boxes do dÃ©bito automÃ¡tico e conta online, conforme status de terminais 55772
function boxDaccCtOnline(){
	var newDACC = $(".tablesDacc");	

	newDACC.before('<span class="toptablesDacc" />');
	newDACC.after('<span class="bottomtablesDacc" />');
	newDACC.find(".mioloTerminais.fixo").each(function(){
		$(this).before('<span class="bottomtablesDacc" />');
		$(this).before('<span class="toptablesDacc" />');
		if($(this).find(".rowspan2 input").is(':visible') == false){
			$(this).children().addClass("nth-childBt");
			$(this).prev().hide();
			$(this).prev().prev().hide();
			$(this).before('<span class="miolotablesDacc" />');
		}
	});
	
	var titOCT = newDACC.parents().find(".tit-contaTotal");
	if(titOCT.is(":visible") == true){   
	    newDACC.addClass("OCT");    
	  
	    var $divMiolo = newDACC.find(".mioloTerminais");
	    			
		$divMiolo.each(function(){
			    var $divExtraFixo = newDACC.find(".mioloTerminais.fixo");
			    var $textoFixo = $(this).find(".terminalFixo");
			    
			    if($textoFixo.is(":visible")){
				       
				    if($divExtraFixo.is(":visible")){
				     
				     		var miolo1 = $divExtraFixo.parent().find('.mioloTerminais:first')
				     		var fixoExtra = miolo1.find('.wRowspan li:first b:first').html();
							$textoFixo.html(fixoExtra)
					}else{	
						var fixoExtra = $divMiolo.find('.wRowspan li:first b:first').html();
							$textoFixo.html(fixoExtra)
		            
					}		
			    }   
	    });			
	}
}

//bot Alguma Dúvida
window.setTimeout(function() {
console.log("bot minha oi");

var dddAjuda = $.cookie("ddd");
if (dddAjuda == "27" || dddAjuda == "28" || dddAjuda == "33" || dddAjuda == "87"){
	$('body').append("<div class='tire-duvidas'><script id='dtbot-script' src='//dtbot.directtalk.com.br/1.0/staticbot/dist/js/dtbot.js?token=b467a598-cec9-499d-b593-72842b979d3a&widget=true&tab=true&top=40&text=Alguma%20d%C3%BAvida%3F&textcolor=ffffff&bgcolor=4E1D3A&from=right'></script></div>");
}

}, 1000);






