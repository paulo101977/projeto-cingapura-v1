$(document).ready(function(){


  $( ".edit-apelido" ).on( "click", function() {
    $(".lightbox").show();
  });

  $( '#overlay, #close').on('click', function(event) {
    $(".lightbox").hide();
  });

  // //when the <submit> button is clicked
  //     $('button.save-apelido').click(function(){
  //
  //       //store the user's entry in a variable
  //   var enteredName = document.getElementById('value-input-apelido-0').value;
  //
  //   //update the html in the second form to show the user's entered name
  //   $('.novo-apelido').text(enteredName);
  //    });

  $("button.save-apelido").click(function(){
    var enteredFirstName = document.getElementById('value-input-apelido-0').value;
    var enteredSecondName = document.getElementById('value-input-apelido-1').value;
    var enteredThirdName = document.getElementById('value-input-apelido-2').value;
    var enteredFourthName = document.getElementById('value-input-apelido-3').value;
    var enteredFifthName = document.getElementById('value-input-apelido-4').value;

    $("#novo-apelido-0").text(enteredFirstName);
    $("#novo-apelido-1").text(enteredSecondName);
    $("#novo-apelido-2").text(enteredThirdName);
    $("#novo-apelido-3").text(enteredFourthName);
    $("#novo-apelido-4").text(enteredFifthName);

    if(enteredFirstName){
      $("#novo-apelido-0").addClass("apelido-block");
      $("#novo-apelido-0").siblings(".phone").addClass("phone-edit-apelido");
      $("#novo-apelido-0").siblings(".widget").addClass("widget-edit-apelido");
      $(".success-message-info.editar-apelido").removeClass("dipnone");
      $(".message__warn-home.editar-apelido").addClass("dipnone");

    }
    if(enteredSecondName){
      $("#novo-apelido-1").addClass("apelido-block");
      $("#novo-apelido-1").siblings(".phone").addClass("phone-edit-apelido");
      $("#novo-apelido-1").siblings(".widget").addClass("widget-edit-apelido");
    }
    if(enteredThirdName){
      $("#novo-apelido-2").addClass("apelido-block");
      $("#novo-apelido-2").siblings(".phone").addClass("phone-edit-apelido");
      $("#novo-apelido-2").siblings(".widget").addClass("widget-edit-apelido");
    }
    if(enteredFourthName){
      $("#novo-apelido-3").addClass("apelido-block");
      $("#novo-apelido-3").siblings(".phone").addClass("phone-edit-apelido");
      $("#novo-apelido-3").siblings(".widget").addClass("widget-edit-apelido");
    }
    if(enteredFifthName){
      $("#novo-apelido-4").addClass("apelido-block");
      $("#novo-apelido-4").siblings(".phone").addClass("phone-edit-apelido");
      $("#novo-apelido-4").siblings(".widget").addClass("widget-edit-apelido");
    }

    // $(".apelido-0 input").clone().appendTo("#chart0");
    // $(".apelido-1 input").clone().appendTo("#chart1");
    $(".lightbox").hide();
   });

});
