$(document).ready(function(){
    function formatNumberComma (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }
  
    function formatNumber(num){
       num = num/1000;
  
      if(num == parseInt(num)){
        return formatNumberComma(parseInt(num));
      }
  
      return formatNumberComma(num.toFixed(2));
    }
  
    //server side values
    var initialUserIncreaseValues = {
      'minIncreaseValue': 50, //min value
      'usersIncreaseInfo': [
        //first
        {
          phone: '(21) 98712-0921',
          value: 2000,
          bloqueado: false
        },
        //second
        {
          phone: '(21) 92812-0992',
          value: 1000,
          bloqueado: false
        },
        //third
        {
          phone: '(21) 98712-0921',
          value: 1000,
          bloqueado: false
        },
        //fourth
        {
          phone: '(21) 99912-2912',
          value: 1000,
          bloqueado: false
        },
        //fifth
        {
          phone: '(21) 98712-0921',
          value: 2000,
          bloqueado: true
        }
      ]
    }
  
  var currentIncreaseValues = [];
  
  var usersIncreaseInfo = initialUserIncreaseValues.usersIncreaseInfo;
  var minIncreaseValue = initialUserIncreaseValues.minIncreaseValue;
  var formatIncreaseMinValue = formatNumber(minIncreaseValue);
  var contentIncrease = $(".section__itensAumentarInternet");
  var originalIncreaseBox = $('.item-aumentar-internet');
  
  function addUserItem() {
  
    usersIncreaseInfo.forEach(function(value, index){
      if(index != 0) {
        var box = originalIncreaseBox.clone()
        box.addClass('increase-' + (index + 1))
        box.find('.item-increase-cingapura').attr('id', 'value-box-increase-' + index)
        box.find('.phone-cingapura').text(usersIncreaseInfo[index].phone);
        box.find('.saldo__plano').text(formatNumber(usersIncreaseInfo[index].value) + " GB ");
  
        if(formatNumber(usersIncreaseInfo[index].value) <= formatIncreaseMinValue) {
          box.find('.saldo__plano').addClass("expired");
          box.find('.message__warn').show();
        }
        if(usersIncreaseInfo[index].bloqueado) {
          box.find('.btn-pkg').addClass("dipnone");
        }else {
          box.find('.btn-pkg-indisponivel').addClass("dipnone");
        }
        contentIncrease.append(box)
      }
    })
    originalIncreaseBox.addClass('increase-1');
    originalIncreaseBox.find('.item-increase-cingapura').attr('id', 'value-box-increase-0');
    originalIncreaseBox.find('.phone-cingapura').text(usersIncreaseInfo[0].phone);
    originalIncreaseBox.find('.saldo__plano').text(formatNumber(usersIncreaseInfo[0].value) + " GB ");
  
    if(formatNumber(usersIncreaseInfo[0].value) <= formatIncreaseMinValue) {
      originalIncreaseBox.find('.saldo__plano').addClass("expired");
      originalIncreaseBox.find('.message__warn').show();
    }
    if(usersIncreaseInfo[0].bloqueado) {
      originalIncreaseBox.find('.btn-pkg').addClass("dipnone");
    }else {
      originalIncreaseBox.find('.btn-pkg-indisponivel').addClass("dipnone");
    }
  }
  
  addUserItem();
  
  });
  