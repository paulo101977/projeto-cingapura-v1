


var dataUserMapValues = [4000, 6000, 10000, 10000, 10000];//initial values
var phonesNumber = [
              "(21) 92812-0991" ,
              "(21) 92812-0993" ,
              "(21) 92812-0994" ,
              "(21) 92812-0995" ,
              "(21) 92812-0992"
            ]
var maxValue = 40000;
var itemSize = 18;//border
var beforeValue = 0;
var previusValue = [];
var graphicStep = 100;
var colors = ['#F8562C' , '#9F2AFF' , '#D82482' , '#FFD700' , '#00baf7' , '#00BAF7'];
var componentWidth = 0;

var currentUserValues = dataUserMapValues.slice();//current data

function getDirection(index , value){
  if(previusValue[index] > value){
    return 'left';
  }

  if(previusValue[index] < value){
    return 'right';
  }

  return '';
}

function checkIfHasCollision(current,before){

  var  value = parseFloat(current.val());
  var beforeValue =   parseFloat(before.val()) ;

  //first or last
  if(isNaN(value) || isNaN(beforeValue)) return false;

  if(value + graphicStep > beforeValue ){
    //console.log('has collision')
    return true;
  }
  //console.log('dont has collision')
  return false;
}

function getSinbling(index){
  return $('#rangeMultiple' + index)
}

function changeInputBGColor(){
  var bg = '-webkit-linear-gradient(left,';

  $('input[type=range][multiple]').each(function(index){
    var el = $(this);

    var elPercent = itemSize/componentWidth*100;
    var totalSize = 100 + elPercent*(dataUserMapValues.length - 2);
    var compSize = (el.val()/maxValue)*(totalSize) - elPercent*(index + 1);

    if(index == 0){
      bg += getBGSegmentColor(0, compSize , colors[index])
    } else if(index == (dataUserMapValues.length - 2)){
      var beforeIndex = index - 1;
      var beforeEl = $('#rangeMultiple' + (beforeIndex));

      bg += ', ' + getBGSegmentColor( (beforeEl.val()/maxValue)*(totalSize), compSize , colors[index])
      bg += ', ' + getBGSegmentColor((el.val()/maxValue)*(totalSize), totalSize , colors[index + 1])
      bg += ')'
    }
    else { //middle item
      var beforeEl = $('#rangeMultiple' + (--index));

     bg += ', ' + getBGSegmentColor((beforeEl.val()/maxValue )*(totalSize) - elPercent*(index + 1),  compSize , colors[++index])
    }
  })

  $('#range').css('background', bg)
}

function getBGSegmentColor(initial, final, color){
  return color + ' ' + initial + '% ,' + color + ' ' + final + '%';
}

function changePositions($__direction , $__index, $object){
  //console.log($__direction)
  var $__sinbling = null;

  if($__direction == 'right'){
    var $__nextIndex = parseInt($__index) + 1;
    $__sinbling = getSinbling($__nextIndex);
    var __collision = checkIfHasCollision($object,$__sinbling)

    if(__collision){
      var $__sinblingValue = parseInt($object.val()) + graphicStep;
      //console.log($__sinblingValue)
      $__sinbling.val($__sinblingValue)
      $__sinbling.change();
    }
  } else {
    var $__beforeIndex = $__index - 1;
    $__sinbling = getSinbling($__beforeIndex);
    var __collision = checkIfHasCollision($__sinbling,$object)

    if(__collision){
      var $__sinblingValue = parseInt($object.val()) - graphicStep;
      $__sinbling.val($__sinblingValue);
      $__sinbling.change();
    }
  }

  //if($__sinbling) $__sinbling.change();
}

function getLabel(index){
  return '<div class="label-item" id="label-item' + index + '">'
            + '<div class="label-number">' + getFormatedValue(currentUserValues[index]) + '</div>'
          +'</div>'
}

function getTooltip($index){

  var arrowClass = $index % 2 == 0 ? 'up' : 'down';

  return '<div class="c-tooltip-info-redis '
            + arrowClass +'" id="tooltip-info-redis' + $index + '">'
            + '<div class="arrow"></div>'
            + '<div class="content">' + getFormatedValue(currentUserValues[$index]) + '</div>'
          + '</div>'

}

function appendBox(){
  var container = $('.c-box__redistribuir');
  var originalBox = $('.c-box__redistribuir .box__redistribuir');
  //var originalBoxClone = originalBox.clone();

  currentUserValues.forEach(function(value, index){
    //console.log('index' , index)
    if(index != 0) {
      var box = originalBox.clone()
      box.addClass('color-' + (index + 1))
      box.find('span').attr('id', 'value-box-' + index)
      box.find('.c-phone').text(phonesNumber[index]);
      container.append(box)
    }
  })

  originalBox.find('span').attr('id', 'value-box-0')
  originalBox.find('.c-phone').text(phonesNumber[0]);
  originalBox.addClass('color-1')
}

function appendLabelsAndTooltip(){
  var $__labelContainer = $('.c-label-container');

  $('input[type=range][multiple]').each(function(){
    var $__current = $(this);
    var $__value = parseInt($__current.val());
    var $__index = parseInt($__current.attr('id').split('rangeMultiple')[1]);

    var __labelComp = $(getLabel($__index,$__value));
    var __tooltip = $(getTooltip($__index))

    $__labelContainer.append(__labelComp);
    $__labelContainer.append(__tooltip);

    if($__index == currentUserValues.length - 2){
      //last
      var lastIndex = $__index + 1;
      var __labelLast = $(getLabel(lastIndex,parseInt(maxValue - $__value)));
      var __lastTooltip = $(getTooltip(lastIndex));

      $__labelContainer.append(__labelLast);
      $__labelContainer.append(__lastTooltip);
    }

  })
}

function setVisibleInfo(posX , posBefore, __labelComp, __tooltip, itemSize, index){
  if((posX - posBefore < __labelComp.width()) && index == 0){
    __labelComp.hide();
    __tooltip.show();
  }
  else if(posX - posBefore < __labelComp.width() + itemSize){
    __labelComp.hide();
    __tooltip.show();
  }
  else {
    __labelComp.show();
    __tooltip.hide();
  }
}

function updatePositionLabelsAndTooltips(){

  $('input[type=range][multiple]').each(function(){
    var $__current = $(this);
    var $__value = parseInt($__current.val());
    var $__index = parseInt($__current.attr('id').split('rangeMultiple')[1]);
    var $__before = $('#rangeMultiple' + ($__index - 1));

    //var __value = currentUserValues[$__index];

    //reference
    var __labelComp = $('#label-item' + $__index);
    var __tooltip = $('#tooltip-info-redis' + $__index);

    var $__beforeValue = parseInt($__before.val());

    if(isNaN($__beforeValue)) $__beforeValue = 0;

    //ok
    var relativeCompWidth =  componentWidth - (itemSize*(currentUserValues.length - 1));
    //relativeCompWidth = relativeCompWidth - itemSize;

    //margin + relative width
    var posX = 0;

    if($__index == 0){
      var relativeValuePos = $__value*relativeCompWidth/maxValue;
      posX = (relativeValuePos - __labelComp.width())/2;

      //background
      $('.range-bg-' + $__index).width(relativeValuePos)

      setVisibleInfo(relativeValuePos , 0, __labelComp, __tooltip, itemSize, $__index)
      __labelComp.css({left: posX})

      posX = ((relativeValuePos - itemSize/2) - (__tooltip.width() + 2))/2;

      __tooltip.css({left: posX})

      //console.log('first')
      //two values
      if(currentUserValues.length == 2){
        //last
        var posLast = componentWidth;
        var lastIndex = $__index + 1;
        var __labelLast = $('#label-item' + lastIndex);
        var __lastTooltip = $('#tooltip-info-redis' + lastIndex);

        posX = itemSize*(lastIndex - 1)+ $__value*relativeCompWidth/maxValue;

        //background
        $('.range-bg-' + lastIndex).width(posLast - posX)
        setVisibleInfo(posLast , posX, __labelLast, __lastTooltip, itemSize, lastIndex);

        relativeValuePos = posX + (posLast - posX)/2;

        posX = relativeValuePos + itemSize/2 - __labelComp.width()/2;

        __labelLast.css({left:posX})

        posX = relativeValuePos - (__lastTooltip.width() + 2)/2;

        __lastTooltip.css({left:posX})
      }
    }
    else if($__index == currentUserValues.length - 2){
      //before last
      posX = itemSize*$__index + $__value*relativeCompWidth/maxValue;
      var posBefore = (itemSize*($__index - 1)) + ($__beforeValue*relativeCompWidth/maxValue);
      var __tooltip = $('#tooltip-info-redis' + $__index);
      var relativeValuePos =  posBefore + (posX - posBefore)/2;
      //background
      $('.range-bg-' + $__index).width(posX - posBefore)

      setVisibleInfo(posX , posBefore, __labelComp, __tooltip, itemSize, $__index);

      posX = relativeValuePos + itemSize/2 - __labelComp.width()/2;
      __labelComp.css({left: posX})

      posX = relativeValuePos - (__tooltip.width() + 2)/2;
      __tooltip.css({left: posX})

      //last
      var posLast = componentWidth;
      var lastIndex = $__index + 1;
      var __labelLast = $('#label-item' + lastIndex);
      var __lastTooltip = $('#tooltip-info-redis' + lastIndex);
      var relativeValuePos = 0;

      posX = itemSize*(lastIndex - 1)+ $__value*relativeCompWidth/maxValue;

      //background
      $('.range-bg-' + lastIndex).width(posLast - posX)
      setVisibleInfo(posLast , posX, __labelLast, __lastTooltip, itemSize, lastIndex);

      relativeValuePos = posX + (posLast - posX)/2;

      posX = relativeValuePos + itemSize/2 - __labelLast.width()/2;
      __labelLast.css({left:posX})

      posX = relativeValuePos - (__lastTooltip.width() + 2)/2;
      __lastTooltip.css({left:posX})

    }
    else {
      posX = (itemSize)*$__index  + $__value*relativeCompWidth/maxValue;
      var posBefore = (itemSize*($__index - 1))  + ($__beforeValue*relativeCompWidth/maxValue);
      var relativeValuePos = posBefore + (posX - posBefore)/2;

      //background
      $('.range-bg-' + $__index).width(posX - posBefore)

      //tooltip/label visible
      setVisibleInfo(posX , posBefore, __labelComp, __tooltip, itemSize, $__index);

      var posLabel = relativeValuePos + itemSize/2  - __labelComp.width()/2;

      //console.log('posLabel', posLabel)
      __labelComp.css({left: posLabel})

      var posTooltip = relativeValuePos - (__tooltip.width() + 2)/2;

      //console.log('posTooltip',relativeCompWidth)
      __tooltip.css({left: posTooltip})

    }
  })
}

//fix IE
function appendThumbListener(){
  var drag = d3.behavior.drag();
    //selection.call(drag);
  var value = 0;

    $('.range-thumb').each(function(){
      var index = parseInt($(this).attr('id').split('thumb')[1]);

      var  x = 0;
      var realX = 0;
      var range = $('#rangeMultiple' + index);
      var width = range.width() - itemSize;

      value += currentUserValues[index];
      realX =  value/maxValue*(width); //without margin
      x =  value/maxValue*(width) + (itemSize*index); //add margin

      //thumb initial
      var thumb =
          d3.selectAll('#thumb' + index)
      .data([ {"x":realX, "y":0} ])
      .style("left",  x + "px")
      .call(drag);

      //console.log('index', index)

      //after drag
      drag.on('drag', function(d,i){
        var index = parseInt(d3.select(this).attr('id').split('thumb')[1]);
        var range = $('#rangeMultiple' + index);
        var width = range.width() - itemSize;

        var graphicStepWidth = (graphicStep*width/maxValue);
        var valueMinWidth = graphicStepWidth*(index + 1);
        var valueMaxWidth = graphicStepWidth*((currentUserValues.length - 1) - index)

        //console.log(valueMinWidth)
        d.x += parseInt(d3.event.dx);

        //d.x = d.x ;
        //console.log(d.x)
        if(d.x < valueMinWidth) d.x = valueMinWidth;

        if(d.x > width - valueMaxWidth) d.x = (width - valueMaxWidth);

        var value = d.x/(width)*maxValue;

        range.val(value)
        range.change();
        d3
          .select(this)
          .style("left", (parseInt(d.x) + itemSize*index )+ "px")
      })
    })

}


function setInitial(){
  var $container = $('.c-range-container');

  //set max value
  $('.user-max-value').text(parseInt(maxValue/1000));

  appendBox();

  for(i = 0; i < dataUserMapValues.length - 1; i++){
    var input = $('<input type="range" multiple></input>').clone();
    var thumb = $('<div class="range-thumb"></div>')
    thumb.attr('id','thumb' + i);

    $container.append(thumb);

    previusValue.push(0);

    input.attr("max" , maxValue)
    input.attr("id" , "rangeMultiple" + i);
    input.attr( "min" , 0);
    input.attr( "step" , graphicStep);
    input.val(dataUserMapValues[i] + beforeValue)
    input.css({'marginLeft': itemSize*i})
    $container.append(input);
    input.width($('#range').width() - itemSize*(dataUserMapValues.length - 2))


    //set value
    $('#value-box-' + i).text(getFormatedValue(currentUserValues[i]));
    if(i == currentUserValues.length - 2){
      var lastIndex = i + 1;
      $('#value-box-' + lastIndex).text(getFormatedValue(currentUserValues[lastIndex]));
    }


    beforeValue= parseFloat(input.val());

    //$('#range').width(input.width() + itemSize*(dataValues.length - 2));

    componentWidth = $('#range').width();

    //change thumb position
    var currentValue = currentUserValues[i];

    //var relWidth = componentWidth - (itemSize*(currentUserValues.length - 1));
    //var x =  currentValue/maxValue*(relWidth)

    //console.log(x)

    //changeThumb(x, i, currentValue)
  }

  appendLabelsAndTooltip();
  updatePositionLabelsAndTooltips();
}

function gigaInfo(output){
  output = output/1000

  if(parseInt(output) == (output)){
    return ( parseInt(output) + " GB");
  }

  return ((output).toFixed(1) + " GB");
}

function getFormatedValue(output){
  //console.log('isInteger' , Number.isInteger((output/1000).toFixed(1) ))
  //return (output > 999) ? (gigaInfo(output)) : (parseInt(output) + " MB")
  return gigaInfo(output);
}

function getSumWidthoutThisPosition(position){
  var sum = 0;

  currentUserValues.forEach(function(value,index){
    if(index != position) sum += parseInt(value);
  })


  return sum;
}

var getTotalSum = function(){
  var sum = 0;

  currentUserValues.forEach(function(value,index){
    sum += parseInt(value);
  })

  return sum;
}

var setLabelsAndValues = function(index, current, nextVal){
  var nextIndex = parseInt(index) + 1;
  currentUserValues[index] = current;
  currentUserValues[nextIndex] = nextVal;
  $('#value-box-' + index).text(getFormatedValue(current));
  $('#value-box-' + (nextIndex)).text(getFormatedValue(nextVal));
  $('#label-item' + index + ' .label-number').text(getFormatedValue(current));
  $('#label-item' + nextIndex + ' .label-number').text(getFormatedValue(nextVal));
  $('#tooltip-info-redis' + index + ' .content').text(getFormatedValue(current));
  $('#tooltip-info-redis' + nextIndex + ' .content').text(getFormatedValue(nextVal));
}

function updateValues(index, $__value){

  $__value = parseInt($__value);
  index = parseInt(index);
  var nextIndex = (index) + 1;
  var previusIndex = (index) - 1;
  var next = $('#rangeMultiple' + nextIndex);
  var nextVal = parseInt(next.val());
  var previus = $('#rangeMultiple' + previusIndex);
  var previusValue = parseInt(previus.val());


  if(index == 0){
    nextVal = nextVal - $__value;

    if(currentUserValues.length > 2){
      setLabelsAndValues(index, $__value, nextVal)
    }
    else {
      nextVal = maxValue - $__value;
      setLabelsAndValues(index, $__value, nextVal)
    }

  }
  //last
  else if(index + 2 == currentUserValues.length){
    var current = $__value - previusValue;
    nextVal = maxValue - $__value;
    setLabelsAndValues(index, current, nextVal)

    //console.log('last')

  } else {//middle
    var current = $__value - previusValue;
    nextVal = nextVal - $__value;
    setLabelsAndValues(index, current, nextVal)

    //console.log('middle')
  }

}

function collisionOnStartAndEnd($__index, $__value){
  var $__comp = $('#rangeMultiple' + $__index);

  var first = parseInt($__index) + 1;
  var last = (currentUserValues.length - 1) - parseInt($__index);


  //start
  if($__value < graphicStep*first){
    $__comp.val(graphicStep*first);
    $__comp.change();
  }

  //end
  if($__value > maxValue - graphicStep*last){
    $__comp.val(maxValue - graphicStep*last);
    $__comp.change();
  }
}

function changeThumb(x , index,value){
  var dx = x + itemSize*index;
  $('#thumb' + index).css({left:dx})
  d3
    .select('#thumb' + index)
    .datum({"x":x})
}

$(window).ready(function(){
  setInitial();

  //append listeners to thumbs
  appendThumbListener();

  $('#restaurar__saldo').click(function(event){
    event.preventDefault();
    event.stopPropagation();

    var current = 0;

    dataUserMapValues.forEach(function(value, index){
      var comp =   $('#rangeMultiple' + index);
      current += value;

      comp.val(current);
      comp.change();
    })
  })

  //changeInputBGColor();
  $('#write-button').click(function(event){
    event.preventDefault();
    event.stopPropagation();

    var max = maxValue;
    var current = 0;

    //var currentUserValues
    var value = parseInt(maxValue/currentUserValues.length);
    value = (value/1000).toFixed(1)*1000;
    //console.log(value)

    for(i = 0; i < currentUserValues.length ; i++){
      var comp =   $('#rangeMultiple' + i);

      currentUserValues[i] = value;
      max -= value;

      if(i == currentUserValues.length - 1){
        if(max > 0 || max < 0) currentUserValues[i] += max;
      }

      current += currentUserValues[i];
      comp.val(current);
      comp.change();
    }

  })

  $('input[type=range][multiple]').each(function(){
    //console.log($(this).attr('id'))
    var id = $(this).attr('id');

    //console.log(id)

    $(document).on('input change', "#" + id , function() {

        var $__value = parseInt($(this).val());
        var $__index = $(this).attr('id').split('rangeMultiple')[1];
        var $__direction = getDirection($__index , $__value);
        //console.log($__direction)

        //test collision and change position
        changePositions($__direction , $__index, $(this));

        //set previus
        previusValue[$__index] = $__value;

        updateValues($__index, $__value);
        updatePositionLabelsAndTooltips();

        collisionOnStartAndEnd($__index, $__value)

        var relWidth = componentWidth - (itemSize*(currentUserValues.length - 1));
        var x =  $__value/maxValue*(relWidth)


        changeThumb(x, $__index, $__value)

        //changeInputBGColor()

    })
  })
})
