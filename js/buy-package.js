$(document).ready(function(){
  function formatNumberComma (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
  }

  function formatNumber(num){
     num = num/1000;

    if(num == parseInt(num)){
      return formatNumberComma(parseInt(num));
    }

    return formatNumberComma(num.toFixed(2));
  }

  //server side values
  var initialUserPkgValues = {
    'minPkgValue': 50, //min value
    'usersPkgInfo': [
      //first
      {
        name: 'Marianna',
        phone: '(21) 98712-0921',
        value: 50,
        totalPlano: 2000,
        media: 21,
        bloqueado: false
      },
      //second
      {
        name: 'Marcela',
        phone: '(21) 92812-0992',
        value: 950,
        totalPlano: 1000,
        media: 21,
        bloqueado: false
      },
      //third
      {
        name: 'Ludmilla',
        phone: '(21) 98712-0921',
        value: 2800,
        totalPlano: 5000,
        media: 228,
        bloqueado: false
      },
      //fourth
      {
        name: 'Henrique',
        phone: '(21) 99912-2912',
        value: 3100,
        totalPlano: 5000,
        media: 1,
        bloqueado: false
      },
      //fifth
      {
        name: 'Fernando',
        phone: '(21) 98712-0921',
        value: 2800,
        totalPlano: 5000,
        media: 391,
        bloqueado: true
      }
    ]
  }

var currentPkgValues = [];

var usersPkgInfo = initialUserPkgValues.usersPkgInfo;
var minPkgValue = initialUserPkgValues.minPkgValue;
var formatPkgMinValue = formatNumber(minPkgValue);
var contentPkg = $(".section__itensCompraPacote");
var originalPkgBox = $('.item-plano-cingapura');

function addAtor() {

  usersPkgInfo.forEach(function(value, index){
    if(index != 0) {
      var box = originalPkgBox.clone()
      box.addClass('pkg-' + (index + 1))
      box.find('.item-pkg-cingapura').attr('id', 'value-box-pkg-' + index)
      box.find('.name-cingapura').text(usersPkgInfo[index].name);
      box.find('.phone-cingapura').text(usersPkgInfo[index].phone);
      box.find('.saldo__plano').text(formatNumber(usersPkgInfo[index].value) + " GB ");
      box.find('.total__plano').text("/" + " " + formatNumber(usersPkgInfo[index].totalPlano) + " GB ");
      box.find('.media-consumo-cingapura').text((usersPkgInfo[index].media) + " MB por dia ");

      if(formatNumber(usersPkgInfo[index].value) <= formatPkgMinValue) {
        box.find('.saldo__plano').addClass("expired");
        box.find('.message__warn').show();

      }
      if(usersPkgInfo[index].bloqueado) {
        box.find('.btn-pkg').addClass("dipnone");
      }else {
        box.find('.btn-pkg-indisponivel').addClass("dipnone");
      }
      contentPkg.append(box)
    }
  })
  originalPkgBox.addClass('pkg-1');
  originalPkgBox.find('.item-pkg-cingapura').attr('id', 'value-box-pkg-0');
  originalPkgBox.find('.name-cingapura').text(usersPkgInfo[0].name);
  originalPkgBox.find('.phone-cingapura').text(usersPkgInfo[0].phone);
  originalPkgBox.find('.saldo__plano').text(formatNumber(usersPkgInfo[0].value) + " GB ");
  originalPkgBox.find('.total__plano').text("/" + " " + formatNumber(usersPkgInfo[0].totalPlano) + " GB ");
  originalPkgBox.find('.media-consumo-cingapura').text((usersPkgInfo[0].media) + " MB por dia ");

  if(formatNumber(usersPkgInfo[0].value) <= formatPkgMinValue) {
    originalPkgBox.find('.saldo__plano').addClass("expired");
    originalPkgBox.find('.message__warn').show();
  }
  if(usersPkgInfo[0].bloqueado) {
    originalPkgBox.find('.btn-pkg').addClass("dipnone");
  }else {
    originalPkgBox.find('.btn-pkg-indisponivel').addClass("dipnone");
  }
}

addAtor();

});
