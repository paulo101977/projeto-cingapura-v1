//var minValue = 10; //10MB

function formatNumberComma (num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function formatNumber(num){
   num = num/1000;

  if(num == parseInt(num)){
    return formatNumberComma(parseInt(num));
  }

  return formatNumberComma(num.toFixed(2));
}

//server side values
var initialValuesUserbar = {
  'minValue': 10, //min value
  'usersInfo': [
    //left
    {
      name: 'Marianna',
      phone: '(21) 98712-0921',
      value: 10000,
      avulso: 4000,
      bloqueado: true
    },
    //right
    {
      name: 'Ludmilla',
      phone: '(21) 98001-1977',
      value: 10000,
      avulso: 0,
      bloqueado: false
    }
  ]
}

var currentValuesUserbar = [];
var timer = 0;
var exp = 1.10; // 1.02
var totalTime = initialValuesUserbar.usersInfo[0].value + initialValuesUserbar.usersInfo[1].value;

var setInitialBarValues = function(values){
  var usersInfo = values.usersInfo;
  var left = usersInfo[0];
  var right = usersInfo[1];

  var primary = document.getElementById('primary');
  var secondary = document.getElementById('secondary');
  var btnLeft = document.getElementById('btn-left');
  var btnRight = document.getElementById('btn-right');

  var interval;

  currentValuesUserbar.push(left.value)//[0]
  currentValuesUserbar.push(right.value)//[1]

  //$('.warn-message-info.ator-desabilitado').hide();
  $('.blocknumber-left').hide();
  $('.blocknumber-right').hide();

  if(left.bloqueado) $('.blocknumber-left').show();
  if(right.bloqueado) $('.blocknumber-right').show();

  //left
  $('.left-name').text(left.name)
  $('.left-phone').text(left.phone)
  $('.left-value').text(formatNumber(left.value));

  // if(left.avulso) {
  //   $('.left-avulso-value').text("+" + formatNumber(left.avulso) +" GB avulso")
  // }

  $('.right-name').text(right.name)
  $('.right-phone').text(right.phone)
  $('.right-value').text(formatNumber(right.value))

  // if(right.avulso){
  //   $('.right-avulso-value').text("+" + formatNumber(right.avulso) +" GB avulso")
  // }
}

function clearErrorAdd(){
  $('.left-container-avulso').removeClass('nactive')
  $('.right-container-avulso').removeClass('nactive')
  $('.warn-message-info.ator-desabilitado').removeClass('active');
  $('.warn-message-info.ator-desabilitado').addClass('nactive');
  $('.panel-heading__blockNumber').removeClass('dipnone');
}

function showWarningMessage(className){
  $(className).addClass('nactive')
  $('.warn-message-info.ator-desabilitado').removeClass('nactive'); // mensagem
  $('.warn-message-info.ator-desabilitado').addClass('active');
  $('.warn-message-info.ator-desabilitado .phone').text(initialValuesUserbar.usersInfo[0].phone);
  $('.panel-heading__blockNumber').addClass('dipnone');
}

//document ready
$(document).ready(function(){
  var interval;
  var userEventClickId = 0.01;
  var incrementTimeInterval = 200;
  var tooltipUserInfo = 'Pagamento ainda não identificado. Em caso de dúvidas, ligue <b>*144</b> do seu Oi ou'
                        + ' <b>1057</b> de qualquer telefone.';
  

  /*$('.tooltip__blocknumber').addClass('teste-opacity');

  $('.panel-heading__blockNumber').hover(function(){
    $('.tooltip__blocknumber').toggleClass('teste-opacity');
  });*/

  $('[data-toggle="tooltip-user-left"]')
    .tooltip({html: true, title: tooltipUserInfo})


  $('[data-toggle="tooltip-user-right"]')
      .tooltip({html: true, title: tooltipUserInfo})

  
  var tot = parseInt(initialValuesUserbar.usersInfo[0].value) + parseInt(initialValuesUserbar.usersInfo[1].value)

  $('.btn-right').click(function(event){
    event.preventDefault();
   
    
    // minimalValue()
    // incrementRight(true);
  })
  .on('mousedown', function(event) {
      event.stopPropagation();
      event.preventDefault();
      
      // userEventClickId = setInterval(function(){
      //   if(incrementRight()) {
      //     clearInterval(userEventClickId);
      //     return;
      //   }
      // }, 150);

      timer = 0;
      interval = setInterval(function() {
        timer++;
        $('.right-container-avulso').addClass('animate') // efeito roxo no clique rápido
        
        // function incrementRight(hasAnimation){
        //   if(currentValuesUserbar[0] > initialValuesUserbar.minValue){
        //     currentValuesUserbar[0] -= 10;
        //     currentValuesUserbar[1] += 10;
        //     $('.left-value').text(formatNumber(currentValuesUserbar[0]))
        //     $('.right-value').text(formatNumber(currentValuesUserbar[1]))
        
        //     clearErrorAdd();
        
        //     $('.right-container-avulso').addClass('animate')
        
        //     if(hasAnimation){
        //       setTimeout(function(){
        //         $('.right-container-avulso').removeClass('animate')
        //       }, 300)
        //     }
        //   } else {
        //     $('.right-container-avulso').removeClass('animate')
        //     showWarningMessage('.left-container-avulso')
        //     return true;
        //   }
        // }
        
        if (timer > 100) {
          if (currentValuesUserbar[0] > 0.01 && currentValuesUserbar[1] < tot) { 
            currentValuesUserbar[0] -= 50;
            currentValuesUserbar[1] += 50;
            console.log(currentValuesUserbar[0])
            $('.right-value').text(formatNumber(currentValuesUserbar[1]))
            $('.left-value').text(currentValuesUserbar[0] < parseInt(50) ? '0.01' : formatNumber(currentValuesUserbar[0]))

            // teste
            clearErrorAdd();

            $('.right-container-avulso').addClass('animate')
            
            
            // fim teste
          }

          if(currentValuesUserbar[0] < 0.01){
            event.stopPropagation();
            event.preventDefault();
            $('.right-container-avulso').removeClass('animate')
            showWarningMessage('.left-container-avulso')

            } else{
              
              return false;
            }
          
        } 
        return false;

      }, 0);
  })
  .on('mouseup', function(event) {
      event.stopPropagation();
      event.preventDefault();
      
      clearInterval(interval);
      if (timer <= 100) {
        if (currentValuesUserbar[0] > 0.01 && currentValuesUserbar[1] < tot) {
          currentValuesUserbar[1] += 10;
          currentValuesUserbar[0] -= 10;
          
          $('.right-value').text(formatNumber(currentValuesUserbar[1]))
          $('.left-value').text(currentValuesUserbar[0] < parseInt(1) ? '0.01' : formatNumber(currentValuesUserbar[0]))
          

        }

        // $('.right-container-avulso').addClass('animate')

      }
      
      $('.right-container-avulso').removeClass('animate') //remove efeito roxo
  });


  

  $('.btn-left').click(function(event){
    event.preventDefault();
    
    // incrementLeft(true);
  })
  .on('mousedown', function(event) {
      event.stopPropagation();
      event.preventDefault();
      
      // userEventClickId = setInterval(function(){
      //   if(incrementLeft()) {
      //     clearInterval(userEventClickId);
      //     return;
      //   }
      // }, 150);

      timer = 0;
      interval = setInterval(function() {
        timer++;
        $('.left-container-avulso').addClass('animate') // efeito roxo no clique rápido

        if (timer > 100) {
          if (currentValuesUserbar[1] > 0.01 && currentValuesUserbar[0] < tot) {
            currentValuesUserbar[0] += 50;
            currentValuesUserbar[1] -= 50;

            $('.left-value').text(formatNumber(currentValuesUserbar[0]))
            $('.right-value').text(currentValuesUserbar[1] < parseInt(50) ? '0.01' : formatNumber(currentValuesUserbar[1]))
            
            // teste
            clearErrorAdd();

            $('.left-container-avulso').addClass('animate')
            
            
            // fim teste
          }

          if(currentValuesUserbar[1] < 0.01){
            event.stopPropagation();
            event.preventDefault();
            $('.left-container-avulso').removeClass('animate')
            showWarningMessage('.right-container-avulso')

            } else{

              return false;
          }

        } 
        return false;

      }, 0);
  })
  .on('mouseup', function(event) {
      event.stopPropagation();
      event.preventDefault();

      clearInterval(interval);
      if (timer <= 100) {
        if (currentValuesUserbar[1] > 0.01 && currentValuesUserbar[0] < tot) {
          currentValuesUserbar[1] -= 10;
          currentValuesUserbar[0] += 10;
                   
          $('.left-value').text(formatNumber(currentValuesUserbar[0]))
          $('.right-value').text(currentValuesUserbar[1] < parseInt(1) ? '0.01' : formatNumber(currentValuesUserbar[1]))
          
        }
        
      }

      $('.left-container-avulso').removeClass('animate') //remove efeito roxo
  });
  
  
  
  //set initial
  setInitialBarValues(initialValuesUserbar);
})
