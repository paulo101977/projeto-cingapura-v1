var consumptionValue = 3500;
var totalConsumptionValue = 7500;
var values =
{
  'max': 15000, //15GB
  'adesao': totalConsumptionValue,
  'valor': consumptionValue
}

var ex1 = consumptionValue < totalConsumptionValue;
var ex2 = consumptionValue == totalConsumptionValue;

function boxSaldo(){
  if (ex1 == true && ex2 == false) {
    $('#consumiu').addClass('hide-box');
    $('#consumindo').addClass('show-box');
    
  } else if(ex1 == false && ex2 == true) {
    $('#consumiu').addClass('show-box');
    $('#consumindo').addClass('hide-box');
  }
};

var pacoteExtraValues =
{
  'pacoteExtraMax': 15000, //15GB
  'pacoteExtraAdesao': 7500,
  'pacoteExtraValor': 1500
}



var datasets = {
  "0":{
    phone: "(21) 99639-7564",
    apelido: '',
    tipo: 'media',
    avulso: 2000,
    dataset:[
      { tipo: 'disponivel', valor: 1550 },
      { tipo: 'total', valor: 4000 },
      { tipo: 'media', valor:  2300}]
  },
  "1":{
    phone: "(21) 99639-7566",
    apelido: 'roberta',
    tipo: 'esgotando',
    avulso: 0,
    dataset: [
      { tipo: 'disponivel', valor: 230 },
      { tipo: 'total', valor: 4000 },
      { tipo: 'media', valor:  2300}]
  },
  "2":{
    phone: "(21) 99639-8888",
    apelido: '',
    tipo: 'cheio',
    avulso: 2000,
    dataset: [
      { tipo: 'disponivel', valor: 4000 },
      { tipo: 'total', valor: 8000 },
      { tipo: 'media', valor:  2300}]
  },
  "3":{
    phone: "(21) 99639-7580",
    apelido: '',
    tipo: 'cheio',
    avulso: 1000,
    dataset: [
      { tipo: 'disponivel', valor: 5000 },
      { tipo: 'total', valor: 8000 },
      { tipo: 'media', valor:  2300}]
  },
  "4":{
    phone: "(21) 99639-7599",
    apelido: '',
    tipo: 'esgotada',
    avulso: 0,
    dataset: [
      { tipo: 'disponivel', valor: 2220 },
      { tipo: 'total', valor: 4000 },
      { tipo: 'media', valor:  4000}]
  }
}

//var index = 0;


function setLabel(max){
  $(".label-min").text('0 GB')
  $(".label-middle").text(formatNumber(max/2)+ ' GB')
  $(".label-max").text(formatNumber(max) + ' GB')
}

function pacoteExtraSetLabel(max){
  $(".label-min").text('0 GB')
  $(".label-middle").text(formatNumber(max/2)+ ' GB')
  $(".label-max").text(formatNumber(max) + ' GB')
}

function setDataLabel(data){
  $('.current-data .data').text(formatNumber(data) + " GB");
}

function pacoteExtraSetDataLabel(pacoteExtraData){
  $('.pacote-extra-current-data .pacote-extra-data').text(formatNumber(pacoteExtraData) + " GB");
}

function setDataLabelPos(pos){
  var comp = $('.current-data');
  var width = comp.width();
  comp.css({left: pos - width/2 - 2})
}

function pacoteExtraSetDataLabelPos(pos){
  var pacoteExtraComp = $('.pacote-extra-current-data');
  var pacoteExtraWidth = pacoteExtraComp.width();
  pacoteExtraComp.css({left: pos - pacoteExtraWidth/2 - 2})
}




function setInfoButton(pos){
  var info = $('.wrapper-graphic-cingapura .info, .saldo-list .total-consumption li .info, .saldo-list .saldo-list-footer div .info');
  info.toggle();

  // info.css({right: pos/2 - info.width()/2})

  $('[data-toggle="tooltip-info"]').tooltip(
    {
      html: true,
      title:
        'A sua internet é de <strong>' + formatNumber(values.max) + ' GB</strong>. '
        +'A franquia deste mês é proporcional à data de adesão ao plano.'
      });

  $('[data-toggle="tooltip-total-momento"]').tooltip(
    {
      html: true,
      title:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu urna libero. Aliquam lacus dui, faucibus ac porta sit amet, viverra et justo.'
      });
  
  $('[data-toggle="tooltip-alerta-sms"]').tooltip(
    {
      html: true,
      title:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu urna libero. Aliquam lacus dui, faucibus ac porta sit amet, viverra et justo.'
      });

  $('[data-toggle="tooltip-alerta-email"]').tooltip(
    {
      html: true,
      title:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu urna libero. Aliquam lacus dui, faucibus ac porta sit amet, viverra et justo.'
      });
}



$(document).ready(function(){
  var width = $('.container-graphic').width();
  var pacoteExtraWidth = $('.pacote-extra-container-graphic').width();

  boxSaldo();


  var adesao = values.adesao;
  var max = values.max;
  var valor = values.valor;
  var valorComp = $('.valor-atual');
  var adesaoComp = $('.proporcional-adesao');
  var adesaoWidth = 0;
  var valorWidth = 0;
  var contentApelido = $(".modal-ini-minhaoi");
  var originalUserApelido = $('.adicionar-apelido');

  setDataLabel(valor);

  var pacoteExtraAdesao = pacoteExtraValues.pacoteExtraAdesao;
  var pacoteExtraMax = pacoteExtraValues.pacoteExtraMax;
  var pacoteExtraValor = pacoteExtraValues.pacoteExtraValor;
  var pacoteExtraValorComp = $('.pacote-extra-valor-atual');
  var pacoteExtraAdesaoComp = $('.pacote-extra-proporcional-adesao');
  var pacoteExtraAdesaoWidth = 0;
  var pacoteExtraValorWidth = 0;

  pacoteExtraSetDataLabel(pacoteExtraValor);



  //set initial label
  //setLabel(max)

  if(values.adesao){
    //adesaoWidth = width*adesao/max;
    //adesaoComp.width(adesaoWidth)
    //valorWidth = width*valor/max - adesaoWidth;
    valorWidth = width*valor/adesao;
    valorComp.width(valorWidth)

    setLabel(values.adesao)

    setInfoButton(adesaoWidth);
    setDataLabelPos(adesaoWidth + valorWidth)
  }
  else{
    $('i.info').hide();
    setLabel(max)
    valorWidth = width*valor/max;
    valorComp.width(valorWidth)
    setDataLabelPos(valorWidth)
  }


  if(pacoteExtraValues.pacoteExtraAdesao){
    //adesaoWidth = width*adesao/max;
    //adesaoComp.width(adesaoWidth)
    //valorWidth = width*valor/max - adesaoWidth;
    pacoteExtraValorWidth = pacoteExtraWidth*pacoteExtraValor/pacoteExtraAdesao;
    pacoteExtraValorComp.width(pacoteExtraValorWidth)

    pacoteExtraSetLabel(pacoteExtraValues.pacoteExtraAdesao)

    setInfoButton(pacoteExtraAdesaoWidth);
    pacoteExtraSetDataLabelPos(pacoteExtraAdesaoWidth + pacoteExtraValorWidth)
  }


  else{
    $('i.info').hide();
    pacoteExtraSetLabel(pacoteExtraMax)
    pacoteExtraValorWidth = pacoteExtraWidth*pacoteExtraValor/pacoteExtraMax;
    pacoteExtraValorComp.width(pacoteExtraValorWidth)
    pacoteExtraSetDataLabelPos(pacoteExtraValorWidth)
  }

  $('.ver-detalhes-por-pessoa__').click(function(){
    $('#detalhes-por-pessoa__').collapse('toggle')
  })



  //listeners
  $('#detalhes-por-pessoa__').on('hide.bs.collapse', function () {
    $('.ver-detalhes-por-pessoa__').text('+ Detalhes')
  })

  $('#detalhes-por-pessoa__').on('show.bs.collapse', function () {
    $('.ver-detalhes-por-pessoa__').text('- Ocultar')

    if(typeof animateArc  !== 'undefined'){
      //console.log('drawChart' , drawChart)
      $.each( datasets, function( key, item ){
        if(item.tipo != 'erro'){
          animateArc(key, item.dataset, item.avulso)
        }
      })
    }
  })

  //draw pie chart and info
  $.each( datasets, function( key, item ) {

    //chartContainer.append(createComponent(index))
    //console.log('index' , index)
    //console.log('item' , item)
    var index = parseInt(key);

    if(item.tipo == 'cheio'){
      //cria components
      createComponent(
        index, //mude para key 0...length se necessário
        item.phone ,
        datasets,
        item.dataset ,
        item.avulso,
        item.tipo,
        item.apelido
      )
      //desenha o chart
      drawChartComSaldo("#chart" + index , item.dataset , item.avulso , index)

    } else if (item.tipo == 'esgotada' || item.tipo === "esgotando") {
      createComponent(
        index, //mude para key 0...length se necessário
        item.phone ,
        datasets,
        item.dataset ,
        item.avulso,
        item.tipo,
        item.apelido
      )
      // if (item.avulso == 0) {
      //   console.log(item.avulso);
      // }
     drawChartEsgotado("#chart" + index , item.dataset , item.avulso , index)

   } else if (item.tipo == 'media') {

     createComponent(
       index, //mude para key 0...length se necessário
       item.phone ,
       datasets,
       item.dataset ,
       item.avulso,
       item.tipo,
       item.apelido
     )

    drawChartMedia("#chart" + index , item.dataset , item.avulso , index)

  } else {
      drawErro(index);
    }
    //index++;
  });
  createModalApelido(datasets);
  showInfoDetail();
})

//var chartContainer = $(".charts-dados-navegados");

function formatNumberComma (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function formatNumber(num){
    num = num/1000;

    if(num == parseInt(num)){
      return formatNumberComma(parseInt(num));
    }

    return formatNumberComma(num.toFixed(1));
}

function createComponent(index, phone, datasets , dataset, avulso , tipo, apelido){

  var chart = d3.selectAll('.charts-dados-navegados')
    .append('div')
    .attr('class', function(){
      if(index == Object.keys(datasets).length - 1
         && ((index + 1) % 2 == 1))
      {
            return 'col-xs-12 chart-item'
      }

      return 'col-xs-6 chart-item'
    })

  //apelido
  chart.append('div')
    .attr({
      id: 'novo-apelido-' + index,
      class: 'apelido-container'
    })
    .text(apelido);

  // phone
  // chart.append('div')
  //   .attr('class','phone')
  //   .text(phone);

  //donut
  chart.append('div')
    .attr('class','widget')
    .append('div')
    .attr({
      id: 'chart' + index,
      class: 'chart-container'
    })

  //valores
  var info = chart.append('div').attr('class','chart-info')

  var infoDetail =  info.append('div').attr('class','info-detail tool-opacity')

  // var iconHover = "<div class='info material-icons show-popover'>info_outline</div>"
  info.append('div')
    .attr('class','valor')
    .text(formatNumber(dataset[0].valor) + " GB")

  info.append('div')
      .attr('class','phone')
      .text(phone);

  info.append('div')
      .attr('class','info material-icons show-popover')
      .text('info_outline')
  if(avulso == 0){
    infoDetail.html("<span class='total'>Internet mensal<strong> " + formatNumber(dataset[1].valor) + "GB</strong></span><span class='media-consumo'>Consumo mensal<strong> " + formatNumber(dataset[2].valor) + "GB</strong></span><span class='avulso dipnone'>Pacote extra<strong> " + formatNumber(avulso) + "GB</strong></span>")

  }else {
    infoDetail.html("<span class='total'>Internet mensal<strong> " + formatNumber(dataset[1].valor) + "GB</strong></span><span class='media-consumo'>Consumo mensal<strong> " + formatNumber(dataset[2].valor) + "GB</strong></span><span class='avulso'>Pacote extra<strong> " + formatNumber(avulso) + "GB</strong></span>")

  }

  // info.append('div')

    var editApelido = chart.append('p')
      .attr('class','edit-apelido edit-posicao tool-opacity')
      editApelido.append('i')
      .attr('class','material-icons icon-edit-apelido')
      .text('mode_edit')
      var tooltipEditApelido = editApelido.append('span')
        .attr('class','tooltip-edit-apelido')
        tooltipEditApelido.text('Definir apelido');
    $(".phone")
    .mouseenter(function() {
      $(this).siblings('.edit-apelido').removeClass('tool-opacity');
    })
    .mouseleave(function() {
      $(this).siblings('.edit-apelido').addClass('tool-opacity');
    });


  if(tipo === "esgotada" || tipo === "esgotando" || tipo === "media"){
      var message =
          chart.append('div')
                      .attr('class','msg-info')

      //<i class="material-icons">warning</i>
      message.append('i')
        .attr('class','material-icons icon-warning')
        .text('warning')

      var messageContainer = message.append('span')
        .attr('class','msg-info-container')

      var msgTxt = messageContainer.append('span')
        .attr('class', 'msg-txt')

      if(tipo === "esgotada"){
        msgTxt.text('Atenção! Saldo esgotado. ');
      }
      if(tipo === "esgotando") {
        msgTxt.text('Alto consumo de internet.');
      }
      if(tipo === "media") {
        msgTxt.text('Alto consumo de internet.');
        messageContainer.append('br')
        messageContainer.append('a')
          .attr('class','link-add testemedia')
          .attr('href','#')
          .text('Mais internet')
      }
      if(tipo === "esgotada" || tipo === "esgotando"){
        messageContainer.append('a')
          .attr('class','link-add')
          .attr('href','#')
          .text('Mais internet')
      }
  }
  showModal();
}

function drawErro(index){
  var chart = d3.selectAll('.charts-dados-navegados')
    .append('div')
    .attr('class', function(){
      if(index == Object.keys(datasets).length - 1
         && ((index + 1) % 2 == 1))
      {
            return 'col-xs-12 chart-item chart-item-erro'
      }

      return 'col-xs-6 chart-item chart-item-erro'
    })

  chart.append('p')
      .text(
        'Ocorreu um erro ao carregar as suas informações.'
        + ' Em alguns instantes, tente novamente.'
      )

  chart.append('a')
    .attr('class', 'btn btn-default btn-try-again btn-block')
    .text('Tentar novamente');

  //phone
  //chart.append('div')
    //.attr('class','phone')
    //.text(phone);
}

function drawChartMedia(chart , dataset, avulso , index) {
  var w=130,h=130;
  var svg=$("#chart" + index)
      .attr({
          id: 'svg' + index,
          width:w,
          height:h,
          class:'gradiente-media border-hover'
      })
}

function drawChartEsgotado(chart , dataset, avulso , index) {
  var w=130,h=130;
  var svg=$("#chart" + index)
      .attr({
          id: 'svg' + index,
          width:w,
          height:h,
          class:'gradiente-esgotado border-hover'
      })
}

function drawChartComSaldo(chart , dataset, avulso , index){
  var w=130,h=130;
  var svg=$("#chart" + index)
      .attr({
          id: 'svg' + index,
          width:w,
          height:h,
          class:'gradiente-saldo border-hover'
      })
   }

function animateArc(index, dataset, avulso){


     var maxAngle = 360;
     var radiusMax = maxAngle;
     var total = (avulso) ? (dataset[1].valor + avulso) : dataset[1].valor;
     var tweenValue = ((dataset[0].valor)/total*radiusMax);
     var w=130,h=130;
     var outerRadius=w/2;
     var innerRadius=53;

     var arc=d3.svg.arc()
       .outerRadius(outerRadius)
       .innerRadius(innerRadius)
       .cornerRadius(50)
       .startAngle(0)

       //console.log('#svg' , '#svg' + index)


    d3.select('#svg' + index)
         .select('path.foreground')
         .datum(0)//reset


     d3.select('#svg' + index)
       .select('path.foreground')
       .datum(tweenValue)
       .transition()

       .duration(0)
       .attrTween("d", function(d){
         var interpolate = d3.interpolate(this._current, d);
         this._current = interpolate(0);

         return function(t){
             return arc.endAngle(interpolate(t))();
         }
       });
   }

   function createModalApelido(data_sets) {
     var contentApelido = $(".modal-ini-minhaoi-apelido");
     var originalUserApelido = $('.adicionar-apelido');
     var btn = document.createElement("BUTTON");


     $.each( data_sets, function( key, item,index ) {

      // if(item.tipo != 'erro') {
      var index = parseInt(key);
       if(key != 0) {
         var box = originalUserApelido.clone()
         box.addClass('apelido-' + index)
         box.find('.item-apelido').attr('id', 'value-box-apelido-' + index);
         box.find('.input-apelido').attr('id', 'value-input-apelido-' + index);
        //  box.find('.novo-apelido').attr('id', 'novo-apelido-' + key);

         box.find('.phone-apelido').text(item.phone);

         contentApelido.append(box)
       }
    //  }
   })
     originalUserApelido.addClass('apelido-0');
     originalUserApelido.find('.item-apelido').attr('id', 'value-box-apelido-0');
     originalUserApelido.find('.input-apelido').attr('id', 'value-input-apelido-0');
    //  originalUserApelido.find('.novo-apelido').attr('id', 'novo-apelido-0');

     originalUserApelido.find('.phone-apelido').text(data_sets[0].phone);
     btn.className += " btn btn-block btn-default save-apelido ";
     btn.innerHTML += 'Salvar';
     contentApelido.append(btn);
   }

function showInfoDetail() {
  $('.show-popover')
  .mouseover(function(e) {
    $(this).siblings('.info-detail').removeClass('tool-opacity');
    $(this).parent().siblings('.widget').find('.border-hover').addClass('border-hover-ok');
  })
  .mouseleave(function(e) {
    $(this).siblings('.info-detail').addClass('tool-opacity');
    $(this).parent().siblings('.widget').find('.border-hover').removeClass('border-hover-ok');

  });
}
function showModal() {
  $( '#overlay, #close').on('click', function(event) {
    $(".lightbox").hide();
	});
	$('.link-add').on('click', function(event) {
	  $(".lightbox").show();
	});
}
